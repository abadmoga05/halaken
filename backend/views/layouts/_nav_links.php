<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Panel', 'url' => ['/panel'], 'visible'=>!Yii::$app->user->isGuest],
        ['label' => 'Contratos', 'url' => ['/contract'], 'visible'=>!Yii::$app->user->isGuest],
        ['label' => 'Catálogos',
            'url' => ['#'],
            'visible'=>!Yii::$app->user->isGuest,
            'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
            'items' => [
                ['label' => 'Categorías', 'url' => ['/categories']],
                ['label' => 'Proveedores', 'url' => ['/company']],
                ['label' => 'Idiomas', 'url' => ['/languages']],
                ['label' => 'Países', 'url' => ['/country']],
                ['label' => 'Regiones', 'url' => ['/region']],
              ['label' => 'Ciudades', 'url' => ['/city']],
              ['label' => 'Pasajeros', 'url' => ['/tour-pax-type/index']],
                ['label' => 'Usuarios', 'url' => ['/users']],
            ],
        ],
        ['label' => 'Tours', 'url' => ['/tour'], 'visible'=>!Yii::$app->user->isGuest],
        ['label' => 'Reservaciones', 'url' => ['/reservations'], 'visible'=>!Yii::$app->user->isGuest],
        ['label' => 'Reportes', 'url' => ['/reports'], 'visible'=>!Yii::$app->user->isGuest],
        Yii::$app->user->isGuest ? (
        ['label' => 'Acceder', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )
    ],
]);