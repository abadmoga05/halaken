<?php if(Yii::$app->user->identity->isReferred) :?>

    <?=Yii::$app->controller->renderPartial('../layouts/_referred_navigation');?>

<?php elseif(Yii::$app->user->identity->isAgency) :?>

    <?=Yii::$app->controller->renderPartial('../layouts/_agency_navigation');?>
<?php elseif(Yii::$app->user->identity->isContest) :?>

    <?=Yii::$app->controller->renderPartial('../layouts/_contest_navigation');?>
<?php else: ?>

    <div class="content-side content-side-full">


        <ul class="nav-main">

                <li class="nav-main-item">
                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/'])?>">
                        <i class="nav-main-link-icon si si-speedometer"></i>
                        <span class="nav-main-link-name">Panel de control</span>
                    </a>
                </li>
                <?php if (Yii::$app->user->identity->isAdmin):?>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/panel/contact-center'])?>">
                        <i class="nav-main-link-icon si si-phone"></i>
                        <span class="nav-main-link-name">Contact Center</span>
                    </a>
                </li>
                <?php endif; ?>
                <?php if (Yii::$app->user->identity->isAdmin && in_array(Yii::$app->user->identity->email , Yii::$app->params['admin_booking_generate'])):?>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/affiliate-contest/draw'])?>">
                        <i class="nav-main-link-icon si si-clock"></i>
                        <span class="nav-main-link-name">Sorteos</span>
                    </a>
                </li>
                <?php endif; ?>
              <?php if(Yii::$app->user->identity->isAdmin) :?>
                <li class="nav-main-heading">Contratos</li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/contract'])?>">
                        <i class="nav-main-link-icon si si-doc"></i>
                        <span class="nav-main-link-name">Ver todos</span>
                    </a>
                </li>
                <?php endif; ?>
                <li class="nav-main-heading">Productos</li>
                <li class="nav-main-item">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="<?=Yii::$app->urlManager->createUrl(['/tour'])?>">
                        <i class="nav-main-link-icon si si-energy"></i>
                        <span class="nav-main-link-name">Actividades</span>
                    </a>
                    <ul class="nav-main-submenu">
                        <?php if(Yii::$app->user->identity->isAdmin) :?>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/tour/create'])?>">
                                    <span class="nav-main-link-name">Agregar actividad</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/tour/sort'])?>">
                                    <span class="nav-main-link-name">Ordenar actividades</span>
                                </a>
                            </li>
                            <!--<li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/tour/sold'])?>">
                                    <span class="nav-main-link-name">Tours Mas vendidos</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/tour/popular'])?>">
                                    <span class="nav-main-link-name">Tours Mas populares</span>
                                </a>
                            </li>
                        
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/review'])?>">
                                    <span class="nav-main-link-name">Opiniones</span>
                                </a>
                            </li>    -->
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/tour'])?>">
                                    <span class="nav-main-link-name">Ver todas</span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if(!Yii::$app->user->identity->isAdmin) :?>

                        <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/reviews'])?>">
                                    <span class="nav-main-link-name">Opiniones</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/tour'])?>">
                                    <span class="nav-main-link-name">Cerrar Fechas</span>
                                </a>
                            </li>
                        <?php  endif; ?>
                    </ul>
                </li>
                <?php if(Yii::$app->user->identity->isAdmin) :?>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="<?=Yii::$app->urlManager->createUrl(['/tour'])?>">
                            <i class="nav-main-link-icon fa fa-shuttle-van"></i>
                            <span class="nav-main-link-name">Traslados</span>
                        </a>
                        <ul class="nav-main-submenu">
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle/bookings'])?>">
                                    <span class="nav-main-link-name">Reservas</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle'])?>">
                                    <span class="nav-main-link-name">Traslados</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle'])?>">
                                    <span class="nav-main-link-name">Alta Traslado</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle-zone'])?>">
                                    <span class="nav-main-link-name">Zonas</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle/destinations'])?>">
                                    <span class="nav-main-link-name">Ligar ciudades</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon fa fa-brain"></i>
                            <span class="nav-main-link-name">Catálogos</span>
                        </a>
                        <ul class="nav-main-submenu">
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                                <i class="nav-main-link-icon si si-energy"></i>
                                    <span class="nav-main-link-name">Actividades</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/categories'])?>">
                                            <span class="nav-main-link-name">Categorias</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/company'])?>">
                                            <span class="nav-main-link-name">Proveedores</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/agency'])?>">
                                            <span class="nav-main-link-name">Agencias</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/languages'])?>">
                                            <span class="nav-main-link-name">Idiomas</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/country'])?>">
                                            <span class="nav-main-link-name">Paises</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/region'])?>">
                                            <span class="nav-main-link-name">Regiones</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/city'])?>">
                                            <span class="nav-main-link-name">Ciudades</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/tour-pax-type/index'])?>">
                                            <span class="nav-main-link-name">Tipos de viajeros</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/users'])?>">
                                            <span class="nav-main-link-name">Usuarios</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/users'])?>">
                                            <span class="nav-main-link-name">Vehiculos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                                    <i class="nav-main-link-icon fa fa-shuttle-van"></i>    
                                    <span class="nav-main-link-name">Traslados</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle-service-type'])?>">
                                            <span class="nav-main-link-name">Tipo de servicio</span>
                                        </a>
                                    </li>
                                    
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle-vehicles-type'])?>">
                                            <span class="nav-main-link-name">Tipo de Vehiculo</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle-service-level'])?>">
                                            <span class="nav-main-link-name">Clase de servicio</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle-vehicles'])?>">
                                            <span class="nav-main-link-name">Vehiculos</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/shuttle-zone'])?>">
                                            <span class="nav-main-link-name">Zonas</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
                
                <li class="nav-main-item">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                        <i class="nav-main-link-icon fa fa-dollar-sign"></i>
                        <span class="nav-main-link-name">Reservaciones</span>
                    </a>
                    <ul class="nav-main-submenu">
                        <?php if(Yii::$app->user->identity->isAdmin) :?>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/booking'])?>">
                                    <span class="nav-main-link-name">Reservación Manual</span>
                                </a>
                            </li>
                        <?php endif;?>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['bookings-report'])?>">
                                <span class="nav-main-link-name">Ver todas</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['bookings-report/stats', 'date'=>(new DateTime('+1 day'))->format('Y-m-d')])?>">
                                <span class="nav-main-link-name">Para mañana</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['bookings-report/stats', 'gtnt'=>true])?>">
                                <span class="nav-main-link-name">Futuras</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['bookings-report/stats', 'past'=>true])?>">
                                <span class="nav-main-link-name">Pasadas</span>
                            </a>
                        </li>
                       <!-- <li class="nav-main-item">
                            <a class="nav-main-link" href="be_ui_buttons.html">
                                <span class="nav-main-link-name">Comentarios</span>
                            </a>
                        </li>-->
                    </ul>
                </li>

                <li class="nav-main-item">
                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                        <i class="nav-main-link-icon si si-graph"></i>
                        <span class="nav-main-link-name">Reportes</span>
                    </a>
                    <ul class="nav-main-submenu">

                    <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/billing/index'])?>">
                                <span class="nav-main-link-name">Facturación</span>
                            </a>
                        </li>
                        <?php if(Yii::$app->user->identity->isAdmin) :?>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/billing/list'])?>">
                                <span class="nav-main-link-name">Facturas recibidas</span>
                            </a>
                        </li>

                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/report/provider'])?>">
                                <span class="nav-main-link-name">Por proveedor</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/report/activity'])?>">
                                <span class="nav-main-link-name">Por actividad</span>
                            </a>
                        </li>
                        <?php endif ?>
                        <li class="nav-main-item">
                            <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/report/export'])?>">
                                <span class="nav-main-link-name">Exportar</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if(Yii::$app->user->identity->isAdmin) :?>
                    <li class="nav-main-heading">Agencias</li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon si si-wallet"></i>
                            <span class="nav-main-link-name">Comisiones</span>
                        </a>
                        <ul class="nav-main-submenu">
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/agency-commission/view-commissions'])?>">
                                    <span class="nav-main-link-name">Ver registros</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-main-submenu">
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/agency-commission/configurator'])?>">
                                    <span class="nav-main-link-name">Configurador</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon si si-plane"></i>
                            <span class="nav-main-link-name">Tours</span>
                        </a>
                        <ul class="nav-main-submenu">                            
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/reservations/agency-pending'])?>">
                                    <span class="nav-main-link-name">Por cobrar</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon si si-book-open"></i>
                            <span class="nav-main-link-name">Facturas</span>
                        </a>
                       <ul class="nav-main-submenu">
                           <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/billing/list', 'agency'=>'1'])?>">
                                    <span class="nav-main-link-name">Recibidas</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon si si-book-open"></i>
                            <span class="nav-main-link-name">Cupones</span>
                        </a>
                        <ul class="nav-main-submenu">
                           <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/coupons/index', 'agency'=>'1'])?>">
                                    <span class="nav-main-link-name">Reporte</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
                <!-- <li class="nav-main-item">
                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/blogs'])?>">
                        <i class="nav-main-link-icon si si-speedometer"></i>
                        <span class="nav-main-link-name">Blogs</span>
                    </a>
                </li> -->
                <!-- BLOGS -->
                
                <?php if(Yii::$app->user->identity->isAdmin) :?>
                    <li class="nav-main-heading">Blogs</li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="<?=Yii::$app->urlManager->createUrl(['/blog-categories'])?>">
                            <i class="nav-main-link-icon si si-globe"></i>
                            <span class="nav-main-link-name">Inspiración</span>
                        </a>
                        <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/blog-categories'])?>">
                                        <span class="nav-main-link-name">Categorías</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/inspiration-posts'])?>">
                                        <span class="nav-main-link-name">Entradas</span>
                                    </a>
                                </li>
                        </ul>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/attractions'])?>">
                            <i class="nav-main-link-icon si si-support"></i>
                            <span class="nav-main-link-name">Blogs Atracciones</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/promotions'])?>">
                            <i class="nav-main-link-icon si si-support"></i>
                            <span class="nav-main-link-name">Blogs Promociones</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="<?=Yii::$app->urlManager->createUrl(['/blog-categories'])?>">
                            <i class="nav-main-link-icon si si-home"></i>
                            <span class="nav-main-link-name">Blogs Ciudades</span>
                        </a>
                        <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/blogs-ciudades'])?>">
                                        <span class="nav-main-link-name">Blogs Ciudad</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/blog-atraccion-ciudad'])?>">
                                        <span class="nav-main-link-name">Blogs Atracción Ciudad</span>
                                    </a>
                                </li>
                        </ul>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/blogs-categories'])?>">
                            <i class="nav-main-link-icon si si-direction"></i>
                            <span class="nav-main-link-name">Blogs Categorías</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/blogs-regions'])?>">
                            <i class="nav-main-link-icon si si-support"></i>
                            <span class="nav-main-link-name">Blogs Regiones</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/virtuals'])?>">
                            <i class="nav-main-link-icon si si-support"></i>
                            <span class="nav-main-link-name">Blogs Tours Virtuales</span>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(Yii::$app->user->identity->isAdmin) :?>
                <li class="nav-main-heading">Fomas de pago</li>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/payment-gateways'])?>">
                            <i class="nav-main-link-icon far fa-money-bill-alt"></i>
                            <span class="nav-main-link-name">Configuración</span>
                        </a>
                    </li>
                <li class="nav-main-heading">Diseño</li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon si si-tag"></i>
                            <span class="nav-main-link-name">Diseño</span>
                        </a>
                        <ul class="nav-main-submenu">
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/home'])?>">
                                    <span class="nav-main-link-name">Keywords</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                <li class="nav-main-heading">Sistemas</li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon fa fa-desktop"></i>
                            <span class="nav-main-link-name">Ekomi</span>
                        </a>
                        <ul class="nav-main-submenu">
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/system/ekomi-reports'])?>">
                                    <span class="nav-main-link-name">Reportes de comentarios</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/system/ekomi-products'])?>">
                                    <span class="nav-main-link-name">Productos</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="javascript:;">
                            <i class="nav-main-link-icon si si-globe"></i>
                            <span class="nav-main-link-name">Tickets</span>
                        </a>
                        <ul class="nav-main-submenu">
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/landings'])?>">
                                    <span class="nav-main-link-name">Ver Sitios</span>
                                </a>
                            </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/landings/create'])?>">
                                    <span class="nav-main-link-name">Agregar Sitios</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php if (Yii::$app->user->identity->isAdmin && in_array(Yii::$app->user->identity->email , Yii::$app->params['admin_logs_payments_gateway'])):?>
                    <li class="nav-main-item">
                        <a class="nav-main-link" href="<?=Yii::$app->urlManager->createUrl(['/logs-payment-gateway'])?>">
                            <i class="nav-main-link-icon far fa-money-bill-alt"></i>
                            <span class="nav-main-link-name">Pasarelas Logs</span>
                        </a>
                    </li>
                    <?php endif; ?>
                <?php endif; ?>


            </ul>
        </div>

    <?php endif; ?>