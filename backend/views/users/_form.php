<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <? if($updateForm ) : ?>
        <p class="help-block">Deja en blanco la contraseña para no cambiarla.</p>
    <? endif; ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->dropDownList([1=>'Si', 0=>'No'], ['prompt'=>'Seleccione uno']  );?>

    <?= $form->field($model, 'approved')->dropDownList([1=>'Si', 0=>'No'], ['prompt'=>'Seleccione uno']  );?>

    <? if(Yii::$app->user->identity->isAdmin) :?>
        <?= $form->field($model, 'id_user_type')->dropDownList(
            ArrayHelper::map(common\models\UsersType::find()->asArray()->all(), 'id', 'name'),
            ['prompt'=>'Seleccione uno']  );
        ?>

        <div id="section-company" style="display:none;">
            <?= $form->field($model, 'id_company')->dropDownList(
                ArrayHelper::map(common\models\Company::find()->asArray()->all(), 'id', 'name'),
                ['prompt'=>'Seleccione uno']  );
            ?>
        </div>
        
    <? else :?>
    <?= $form->field($model, 'id_company')->hiddenInput(['value'=> $model->id_company])->label(false);?>
    <? endif; ?>


   

    <div class="form-group" >
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
