<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\Users;
/* @var $this yii\web\View */
/* @var $searchModel common\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Todos los usuarios');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $this->beginBlock('page-header'); ?>
<?= Html::encode($this->title) ?>
<?php $this->endBlock(); ?>

<div class="users-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Agregar usuario'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'options'=> [
                'class' => 'pagination pagination-lg',
            ]
        ],
        'rowOptions' => function ($model) {
            switch ($model['active']) {
                case 0 : return ['class' => 'warning'];
                case 1: return ['class' => ''];
                default: return [];
            }
        },
        'columns' => [
            'name',
            'surname',
            //'phone',
            [
                'attribute'=>'active',
                'filter'=>[
                    ''=>'Todos',
                    1=>'Si',
                    0=>'No'
                ],
                'value'=> function ($model) {
                    return $model->active == 1 ? 'Si' : 'No';
               },
                'label' => 'Activado',
            ],
            [
                'attribute'=>'approved',
                'filter'=>[
                    ''=>'Todos',
                    1=>'Si',
                    0=>'No'
                ],
                'value'=> function ($model) {
                    return $model->approved == 1 ? 'Si' : 'No';
               },
                'label' => 'Aprobado',
            ],
            [
                'attribute'=>'id_company',
                'filter'=> ArrayHelper::map(common\models\Company::find()->orderBy(['name' => SORT_ASC,])->all(),'id','name'),
                'label' => 'Empresa',
                'value'=> function ($model){
                    if($model->id_user_type == Users::ADMIN) {
                        return 'Administrador';    
                    } else {
                        return $model->company->name;
                    }
                    
                },
                'visible' => ($_GET['UsersSearch']['id_user_type'] != Users::AGENCY)
            ],
            [
                'attribute'=>'id_user_type',
                'filter'=> ArrayHelper::map(common\models\UsersType::find()->orderBy(['name' => SORT_ASC,])->all(),'id','name'),
                'label' => 'Tipo de usuario',
                'value'=> 'type.name',
                'visible' => Yii::$app->user->identity->isAdmin
            ],
            'email:email',
        
           // 'id_company',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update} {delete}',
                'buttons'=>[
                    'delete' => function ($url, $model) {  
                        $url = Url::to(['users/delete', 'id' => $model->id]);   
                        return  Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title'        => 'delete',
                            'data-confirm' => Yii::t('yii', '¿Esta seguro que desea desactivar a este usuario?'),
                            'data-method'  => 'post',
                        ]);                           
                    }
                ]        
            ],
        ],
    ]); ?>


    <?php Pjax::end(); ?>
</div>
