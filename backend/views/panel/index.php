<?php
/* @var $this yii\web\View */

$this->title = 'Panel de Agencias - ' . Yii::$app->name;
use common\models\TourPrice;
use yii\db\Expression;

?> 

<?php $this->beginBlock('main-container-header'); ?>
<div class="bg-image" style="background-image: url('assets/media/photos/photo8@2x.jpg');">
    <div class="bg-black-50">
        <div class="content content-full text-center">
            <div class="my-3">
                <img class="img-avatar img-avatar-thumb" src="/images/user_icon.png" alt="">
            </div>
            <? if (Yii::$app->user->identity->isAgency) : ?>
            <h1 id="greatting" class="h2 text-white mb-0"><?= Yii::$app->user->identity->name ?> <?= Yii::$app->user->identity->surname ?></h1>
            <h2 style="color:white;"> <?= Yii::$app->user->identity->agency->comertial_name ?></h2>
            <? else : ?>
            <h1 id="greatting" class="h2 text-white mb-0"><?= Yii::$app->user->identity->name ?></h1>
            <? endif ?>
        </div>
    </div>
</div>
<?php $this->endBlock(); ?>



<style>
    #shorcuts {
        margin-left: 0px;
        padding-left: 0px;
    }

    #shorcuts li {
        list-style: none;
        width: 72px;
        width;
        display: inline-block;
        margin-right: 30px;
    }

    #shorcuts li p {
        text-align: center;
    }
</style>


<?php if($isAgency):?>
<div class="content content-narrow">
    <div class="row">
        <div class="col-6 col-md-4 col-lg-3 col-xl-4">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x" href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Ventas del mes</div>
                    <div class="font-size-h2 font-w400 text-dark">
                        <?= isset($commission->total_amount) ? $commission->total_amount : 0 ?> <sup>USD</sup>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3 col-xl-4">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x" href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Porcentaje de comisión</div>
                    <div class="font-size-h2 font-w400 text-dark"> <?= isset($commission->commission) ? $commission->commission : 0 ?>%</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-lg-3 col-xl-4">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x" href="javascript:void(0)">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Total de Comisión</div>
                    <div class="font-size-h2 font-w400 text-dark"> <?= isset($commission->total_commission) ? $commission->total_commission : 0 ?><sup>USD</sup></div>
                </div>
            </a>
        </div>
    </div>
</div>
<? endif; ?>
<div class="content content-boxed">
    <div class="row">
        <div class="col-md-7 col-xl-8">
            <!-- Updates -->
            <ul class="timeline timeline-alt py-0">
                <li class="timeline-event">
                    <div class="timeline-event-icon bg-info">
                        <i class="far fa-calendar-check"></i>
                    </div>
                    
                    <?
                 
                    if( $_GET['actualizar']=="true"){
                     
                        $time = TourPrice::find()->orderBy(['id' => SORT_DESC])->one();
     
                        if ($time->time!=date("Y")."-".date("m")."-".date("d")){
                            Yii::$app->db->createCommand()->truncateTable('tour_price')->execute();
                    
                    //price fareharbor
                            $datos = file_get_contents( urlFareharbor."api/external/v1/companies/?api-app=".appFareharbor."&api-user=".userFareharbor);
                            $companies = json_decode($datos);
                           foreach ($companies as $companie){
                            for($i=0;$i<count($companie);$i++){
                           $items= file_get_contents(urlFareharbor."api/external/v1/companies/".$companie[$i]->shortname."/items/?api-app=".appFareharbor."&api-user=".userFareharbor);
                           $datos= json_decode($items);
                           $fareharborCurrency[]=$companie[$i]->currency;
                          }
                           $fareharbor[] =$datos->items ;
                          
                           }
                         
                           foreach ($fareharbor as $key => $tours) 
                           {
                          
                            for($i=0;$i<count($tours);$i++){
                                foreach($tours[$i]->customer_prototypes as $pax){
                                    $tourPrice = new TourPrice();
                                    $tourPrice->product_code =  (string)$tours[$i]->pk;  
                                    $tourPrice->product_price = $pax->total; 
                                    $tourPrice->currency =$fareharborCurrency[$key];
                                    $tourPrice->time = new Expression('NOW()');
                                    $tourPrice->save(); 
                    
                                }
                                 
                               
                        }
                          }
                    //fin price fareharbor
                    //price rezdy
                            $offset=0;
                            $rezdy=[];
                            $product_number=100;
                            while($product_number==100){
                      
                              $datos = file_get_contents(staging."v1/products/marketplace?negotiatedRates=true&offset=".$offset."&apiKey=".rezdy);
                          
                              $json = json_decode($datos);
                              $offset+=100;
                              $rezdy[]= $json->products;
                              $product_number=count($json->products);
                              
                            }
                    
                    for ($i=0; $i <count($rezdy) ; $i++) { 
                        foreach ($rezdy[$i] as $tours ) {
                            $tourPrice = new TourPrice();
                            $tourPrice->product_price = $tours->advertisedPrice;
                            $tourPrice->product_code = $tours->productCode;
                            $tourPrice->currency = $tours->currency;
                            $tourPrice->time = new Expression('NOW()');
                            $tourPrice->save();
                            foreach ($tours->priceOptions as $price ) {
                                $tourPrice = new TourPrice();
                                $tourPrice->product_code = $price->productCode;
                                $tourPrice->product_price = $price->price;
                                $tourPrice->currency = $tours->currency;
                                $tourPrice->time = new Expression('NOW()');
                                $tourPrice->save();
                            }
                           
                          
                    
                        }
                    }
                    
                    
                    
                    $tourPrice->time=date("Y")."-".date("m")."-".date("d");      
                    $tourPrice->save();
                     //fin price rezdy    
                        }
                    }?>
                    
                  
                    <div class="timeline-event-block block invisible" data-toggle="appear">
                  
                        <div class="block-header">
                            <h3 class="block-title">Últimas reservaciones</h3>
                        </div>
                        <div class="block-content">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>No. de Confirmación</th>
                                        <th>Servicio</th>
                                        <th>Cliente</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <? if (!empty($lastBookings)) : ?>
                                    <? foreach ($lastBookings as $index => $item) : ?>
                                    <tr>
                                        <td>
                                            <a href="<?= Yii::$app->urlManager->createUrl(['invoice', 'confirmation' => $item->confirmation]) ?>"><?= $item->confirmation ?></a>
                                        </td>
                                        <td>
                                            <?= $item->tour->tourDetails->name ?>
                                            <small><?= $item->optional->name ?></small>
                                        </td>
                                        <td>
                                            <?= \common\models\TourBookingLead::findOne(['id_tour_booking' => $item->id])->full_name ?>
                                        </td>
                                    </tr>
                                    <? endforeach; ?>
                                    <? else : ?>
                                    <tr>
                                        <td colspan="3">
                                            Sin reservaciones pendientes
                                        </td>
                                    </tr>
                                    <? endif; ?>
                                </tbody>

                            </table>

                            <a class="btn btn-info" href="<?= Yii::$app->urlManager->createUrl(['bookings-report']) ?>">Ver más &rarr; </a>

                            <br /><br />

                        </div>
                    </div>
                </li>

                <? if (!yii::$app->user->identity->isAgency) : ?>
                <li class="timeline-event">
                    <div class="timeline-event-icon bg-modern">
                        <i class="far fa-comments"></i>
                    </div>
                    <div class="timeline-event-block block invisible" data-toggle="appear">
                        <div class="block-header">
                            <? if (Yii::$app->user->identity->isAdmin) : ?>
                            <h3 class="block-title">Comentarios pendientes de revisión</h3>
                            <? else : ?>
                            <h3 class="block-title">Últimos comentarios</h3>
                            <? endif; ?>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="block-content">
                                <table style="width: 100%;" class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Comentario</th>
                                            <th>Fecha</th>
                                            <th>Pais</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? if ($lastComments == 0) : ?>
                                        <tr>
                                            <td colspan="4">
                                                Sin comentarios pendientes
                                            </td>
                                        </tr>
                                        <? else : ?>
                                        <? foreach ($lastComments as $review) : ?>
                                        <tr>
                                            <td style="width: 70%"><?= $review->comment ?></td>
                                            <td style="width: 15%">
                                                <?= $review->date_created ?>
                                            </td>
                                            <td style="width: 5%; text-align: center;">
                                                <img style="width:16px" src="<?php echo Yii::$app->params['cdn'] . '/images/flags/' . $review->country . '.png' ?>" alt="<?= $review->country ?>" />
                                            </td>
                                        </tr>
                                        <? endforeach; ?>
                                        <? endif; ?>
                                    </tbody>
                                </table>

                                <a class="btn btn-info" href="<?= Yii::$app->urlManager->createUrl(['/reviews']) ?>">Ver más &rarr; </a>
                            </div>
                        </div>
                    </div>
                </li>
                <? endif ?>

            </ul>
            <!-- END Updates -->
        </div>
        <div class="col-md-5 col-xl-4">
            <? if (Yii::$app->user->identity->isAdmin) : ?>
            <!-- Products -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        <i class="fa fa-briefcase text-muted mr-1"></i> Productos
                    </h3>
                </div>
                <div class="block-content">
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-info" href="<?= Yii::$app->urlManager->createUrl(['company']) ?>">
                                <i class="si si-notebook fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">Provedores</div>
                            <div class="font-size-sm">Agrega, edita y elimina los proveedores de tours</div>
                        </div>
                    </div>
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-amethyst mr-2" href="<?= Yii::$app->urlManager->createUrl(['contract']) ?>">
                                <i class="si si-doc fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">Contratos</div>
                            <div class="font-size-sm">Agrega, edita y elimina los contratos de los proveedores</div>
                        </div>
                    </div>
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-city mr-2" href="<?= Yii::$app->urlManager->createUrl(['users']) ?>">
                                <i class="si si-people fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">Usuarios</div>
                            <div class="font-size-sm">Agrega, edita y elimina los usuarios con acceso al sistema</div>
                        </div>
                    </div>
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-modern-dark mr-2" href="<?= Yii::$app->urlManager->createUrl(['tour']) ?>">
                                <i class="si si-camera fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">Tours</div>
                            <div class="font-size-sm">Agrega, edita y elimina los tours del sistema</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Products -->
            <? elseif (Yii::$app->user->identity->isAgency) : ?>
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        <i class="fa fa-briefcase text-muted mr-1"></i> <?= Yii::$app->user->identity->agency->comertial_name ?>
                    </h3>
                </div>
                <div class="block-content">
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-city mr-2" href="<?= Yii::$app->urlManager->createUrl(['bookings-report']) ?>">
                                <i class="fa fa-file-excel fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">
                                <a href="<?= Yii::$app->urlManager->createUrl(['bookings-report']) ?>">Reportes &rightarrow;</a>
                            </div>
                            <div class="font-size-sm">Revisa tus reservas pasadas y por otorgar</div>
                        </div>
                    </div>
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-info" href="#">
                                <i class="si si-notebook fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">
                                <a href="<?= Yii::$app->urlManager->createUrl(['/billing']) ?>">Facturación &rightarrow;</a>
                            </div>
                            <div class="font-size-sm">Reporte de reservas efectivas</div>
                        </div>
                    </div>
                </div>
            </div>
            <? else : ?>

            <!-- Products -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">
                        <i class="fa fa-briefcase text-muted mr-1"></i> <?= Yii::$app->user->identity->company->name ?>
                    </h3>
                </div>
                <div class="block-content">
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-city mr-2" href="<?= Yii::$app->urlManager->createUrl(['bookings-report']) ?>">
                                <i class="fa fa-file-excel fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">
                                <a href="<?= Yii::$app->urlManager->createUrl(['bookings-report']) ?>">Reportes &rightarrow;</a>
                            </div>
                            <div class="font-size-sm">Revisa tus reservas pasadas y por otorgar</div>
                        </div>
                    </div>
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-info" href="<?= Yii::$app->urlManager->createUrl(['/tour']) ?>">
                                <i class="si si-notebook fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">
                                <a href="<?= Yii::$app->urlManager->createUrl(['/tour']) ?>">Cierre de Fechas &rightarrow;</a>
                            </div>
                            <div class="font-size-sm">Cierra fechas de tus experienciencias de manera sencilla</div>
                        </div>
                    </div>
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-amethyst mr-2" href="<?= Yii::$app->urlManager->createUrl(['/reviews']) ?>">
                                <i class="si si-doc fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">
                                <a href="<?= Yii::$app->urlManager->createUrl(['/reviews']) ?>">Opiniones &rightarrow;</a>
                            </div>
                            <div class="font-size-sm">Revisa las opiniones que has recibido sobre tu experiencias</div>
                        </div>
                    </div>
                    <div class="media d-flex align-items-center push">
                        <div class="mr-3">
                            <a class="item item-rounded bg-info" href="#">
                                <i class="si si-notebook fa-2x text-white-75"></i>
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="font-w600">
                                <a href="<?= Yii::$app->urlManager->createUrl(['/billing']) ?>">Facturación &rightarrow;</a>
                            </div>
                            <div class="font-size-sm">Reporte de reservas efectivas</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Products -->

            <? endif; ?>
        </div>
    </div>
</div>
<!-- END Page Content -->