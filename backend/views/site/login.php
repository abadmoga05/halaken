<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Extranet - Login - ' . Yii::$app->params['siteName'];
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">
	body {
		color: #fff;
		background: #a7a7a7;
	}
	.form-control {
        min-height: 41px;
		background: #fff;
		box-shadow: none !important;
		border-color: #e3e3e3;
	}
	.form-control:focus {
		border-color: #70c5c0;
	}
    .form-control, .btn {        
        border-radius: 2px;
    }
	.login-form {
		width: 350px;
		margin: 0 auto;
		padding: 100px 0 30px;		
	}
	.login-form form {
		color: #7a7a7a;
		border-radius: 2px;
    	margin-bottom: 15px;
        font-size: 13px;
        background: #ececec;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;	
        position: relative;	
    }
	.login-form h2 {
		font-size: 22px;
        margin: 35px 0 25px;
    }
	.login-form .avatar {
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -50px;
		width: 95px;
		height: 95px;
		border-radius: 50%;
		z-index: 9;
		background: #70c5c0;
		padding: 15px;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.login-form .avatar img {
		width: 100%;
	}	
    .login-form input[type="checkbox"] {
        margin-top: 2px;
    }
    .login-form .btn {        
        font-size: 16px;
        font-weight: bold;
		background: #70c5c0;
		border: none;
		margin-bottom: 20px;
    }
	.login-form .btn:hover, .login-form .btn:focus {
		background: #50b8b3;
        outline: none !important;
	}    
	.login-form a {
		color: #fff;
		text-decoration: underline;
	}
	.login-form a:hover {
		text-decoration: none;
	}
	.login-form form a {
		color: #7a7a7a;
		text-decoration: none;
	}
	.login-form form a:hover {
		text-decoration: underline;
	}
</style>
<style>
    .wrap {
        background-image: url(/images/cover.jpg);
        background-position: center center;
        background-size: cover;
    }
</style>
<div class="row">
    <div class="col-md-5 sortable-layout"></div>
    <div class="col-md-2 sortable-layout">
    <div class="logo">
        <a href="<?= Yii::$app->urlManager->baseUrl ?>/"><img src="<?= Yii::$app->urlManager->baseUrl ?>/images/user_icon.png" alt="Sistema de administración"/></a>
    </div>
    </div>
    <div class="col-md-5 sortable-layout"></div>
</div>
<div class="row">
    <div class="col-md-1 sortable-layout"></div>
    <div class="col-md-10 sortable-layout">
        <div class="login-form">
            <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'fieldConfig' => [
                        'template' => "{input}\n<div class=\"col-lg-8\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-1 control-label'],
                    ],
                ]); ?>
                <h2 class="text-center">Extranet <?=Yii::$app->params['siteName'];?></h2>
                <div class="form-group">
                    <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class'=>'form-control', 'placeholder'=>'Correo electrónico']) ?>
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Contraseña']) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Acceder', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                </div>
                <div class="clearfix">
                    <label class="pull-left checkbox-inline">
                    <?= $form->field($model, 'rememberMe')->checkbox([
                        'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    ]) ?></label>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="col-md-1 sortable-layout"></div>
</div>
<div class="row">
    <div class="col-md-5 sortable-layout"></div>
    <div class="col-md-2 sortable-layout">
        <!--<p><a class="btn btn-warning btn-block" href="<?= Yii::$app->params['frontUrl'] ?>">Regresar</a></p>-->
    </div>
    <div class="col-md-5 sortable-layout"></div>
</div>
