<?php
return [
    'adminEmail' => 'admin@example.com',
    'siteName'   => 'mi-dominio.com',
    'compilation' => 'Beta v0.1',
    'defaultMargin' => 25
];
