<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
$db = require __DIR__ . '/db.php';


return [
    'id' => 'extranet01',
    'name' => 'Extranet',
    'sourceLanguage'=>'00',
    'language'=>'es',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'mail' => [
            'class'            => 'zyx\phpmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'config'           => [
                'mailer'     => 'smtp',
                'host'       => 'mail.tuexperiencia.com',
                'port'       => '587',
                //'smtpsecure' => 'ssl',
                'smtpauth'   => true,
                'username'   => 'cto@tuexperiencia.com',
                'password'   => 'Huesca0106',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'css' => [
                        'javascript/vendor/jquery-ui-1.12.1-oneui/jquery-ui.min.css',
                        'javascript/vendor/jquery-ui-1.12.1-oneui/jquery-ui.structure.min.css',
                        'javascript/vendor/jquery-ui-1.12.1-oneui/jquery-ui.theme.min.css'
                    ],
                    'js' => [
                        'v2/js/oneui.core.min.js',
                        'v2/js/oneui.app.min.js',
                        'v2/js/pages/be_ui_icons.min.js',
                        'javascript/vendor/jquery-ui-1.12.1/jquery-ui.min.js',
                    ]
                ],
                'yii\jui\JuiAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        'javascript/vendor/jquery-ui-1.12.1-oneui/jquery-ui.min.js',
                        'v2/js/core/bootstrap.bundle.min.js',
                    ]
                ]
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-extranet',
        ],
        'user' => [
            'identityClass' => 'common\models\Users',
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-extranet', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'extranet',
            'class' => 'yii\web\Session',
            'cookieParams' => ['lifetime' => 1 * 24 *60 * 60]
        ],
        'formatter' => [
            'locale' => 'es',
            'dateFormat' => 'dd/MM/yyyy',
            'timeFormat' => 'short',
            'defaultTimeZone' => 'America/Cancun'
        ],
        'db' => $db,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ]
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
