<?php

namespace backend\controllers;
use Yii;
use backend\helpers\Agency;

class AdminController extends BaseController
{
   
    public function beforeAction($action) {
        $this->layout = 'mainv2';

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                Yii::$app->user->loginUrl = ['/site/login', 'return' => \Yii::$app->request->url];
                return $this->redirect(Yii::$app->user->loginUrl)->send();
            }

            if(Yii::$app->user->identity->isAgency && !in_array($action->controller->id, Agency::AllowSites)){
                return $this->redirect(Yii::$app->urlManager->createUrl(['/panel']))->send();
            }


            if(Yii::$app->user->identity->isReferred && ($action->controller->id != 'panel') ){
                return $this->redirect(Yii::$app->urlManager->createUrl(['/panel']))->send();
            }
            

        }

        return true;
    }
}
