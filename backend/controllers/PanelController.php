<?php

namespace backend\controllers;

use common\models\Contract;
use common\models\Tour;
use common\models\TourBooking;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use common\models\TourReview;
use common\models\TourBookingSearch;
use common\models\TourBookingAgency;
use common\models\Users;
use common\models\contactcenter\CallCenter;
use common\services\contactcenter\ContactCenterService;
use yii\debug\models\search\User;
use common\models\AgencyCommissionRecord;
use common\models\Referred;
use common\helpers\Agency;
use common\models\Agency as _Agency;

use common\models\AffiliateContest;
use common\models\AffiliateContestSearch;

class PanelController extends AdminController
{

    public function actionIndex()
    {
      ini_set('memory_limit', '-1');
      set_time_limit(0);
      //obteniendo basepath
      $urlbase=Yii::$app->basePath;
      $url = str_replace("backend", "", $urlbase);
      //fin de obteniendo basepath
    
      $this->registerJsFiles([
        [
          'url'=> '@web/javascript/v2/greattig.js',
          'position' => \yii\web\View::POS_END,
          'depends' => [\yii\web\JqueryAsset::className()]
        ],
        [
        'url'=> '@web/javascript/panel.index.js',
        'position' => \yii\web\View::POS_END,
        'depends' => [\yii\web\JqueryAsset::className()]
      ]
      ]);


    
      
      return $this->render('index',[
        'url' => $url
      ]);
      //return $this->render('index');
    }

}
