<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

use common\models\LoginForm;

use common\models\User;
use common\models\Users;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'login';

        if (!Yii::$app->user->isGuest){
            return $this->redirect(['/panel'])->send();
        }

        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        $model = new LoginForm();
        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            $user = Users::findOne([
                'email' => $model->username,
            ]);
            if (!password_verify($model->password, $user->password)) {
                Yii::$app->session->setFlash('Usuario o contraseña invalida');
                $this->redirect(['site/login']);
            } else {
                Yii::$app->user->login($user, 0);
                if (Yii::$app->user->identity->isAgency) {
                    if (Yii::$app->user->identity->approved == 0) {
                        Yii::$app->session->setFlash('notice', 'Complete su registro para que tu solicitud pueda ser analizada y posteriormente aceptada!');
                        return $this->redirect(['agency/update', 'id' => Yii::$app->user->identity->id_agency]);
                    }
                    $this->redirect(['panel/index']);
                }else {
                    $this->redirect(['panel/index']);
                }
            }
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
