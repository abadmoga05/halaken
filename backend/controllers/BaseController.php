<?php

namespace backend\controllers;
use Yii;

class BaseController extends \yii\web\Controller
{
   
    /*public function beforeAction($action) {

        if (parent::beforeAction($action)){
            if (Yii::$app->user->isGuest){
                Yii::$app->user->loginUrl = ['/site/login', 'return' => \Yii::$app->request->url];
                return $this->redirect(Yii::$app->user->loginUrl)->send();
            }
        }
        return true;
    }*/


    public function registerJsFiles($jsFiles){


        foreach ($jsFiles as $js ){
            $url = $js['url'];
            Yii::$app->view->registerJsFile( $url, $js);
        }


    }


}
