var row  = 0;
$(document).ready(function(){
    
    $("#select-destination").select2({
        width: '510px',
        ajax: {
            url: 'index.php?r=shuttle/ajax-destinations',
            dataType: 'json'
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });

    $("#select-cities").select2({
        width: '510px',
        ajax: {
            url: 'index.php?r=shuttle/ajax-cities',
            dataType: 'json'
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
    });

    $("#select-cities-search").select2({
        width: '510px',
        ajax: {
            url: 'index.php?r=shuttle/ajax-cities',
            dataType: 'json'
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
    });

    $("#search-relatioship").click(function(){
        if(($("#select-cities-search").val() != null)){
            window.location = "/index.php?r=shuttle/destinations&id_search="+$("#select-cities-search").val();
        }
    })


    $('.btn-add-row').click(function(e){
        e.preventDefault();
        if(($("#select-destination").val() == null) || ($("#select-cities").val() == null)){
            alert('Seleccione ciudad y destino para poder enlazar')
        } else {
            
            $.ajax({
                url:'index.php?r=shuttle/ajax-add-relationship',
                dataType:'json',
                data: {
                    id_zone : $("#select-destination").val(), 
                    id_city : $("#select-cities").val()
                }, 
                success:function(data){
                    console.log(data.exist);
                    if(!data.exist){ // no existe la relación se manda a editar para agregar contenido
                        window.location = "/index.php?r=shuttle-city-zone/relationship&id="+data.id;
                        
                    }else{
                        alert("Este enlace ya ha sido creado anteriormente");
                    }
                },
                error: function(err){
                    alert("Ocurrio un error intentel ode nuevo");
                }
            })
            
        }
    });

    

});


function alerta(id)
    {
    var mensaje;
    var opcion = confirm("Esta seguro de esta acción. Se eliminará el enlace (Origen - Destino) y el contenido que se haya cargado");
    if (opcion == true) {

        ///index.php?r=shuttle-city-zone/delete-relationship&id=
        window.location = "/index.php?r=shuttle-city-zone/delete-relationship&id="+id;
	} 
}

function eliminar_contenido(id,id_content)
    {
    var mensaje;
    var opcion = confirm("Esta seguro de esta acción. Se eliminará el contenido que se haya cargado");
    if (opcion == true) {

        ///index.php?r=shuttle-city-zone/delete-relationship&id=
        window.location = "/index.php?r=shuttle-city-zone/delete-content&id_relationship="+id+"&id_content="+id_content;
	} 
}


