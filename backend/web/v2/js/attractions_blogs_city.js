var index = 0;
$(document).ready(function(){
    console.log("ready");
    $("#tour-chooser").select2({
        ajax: {
            url: '/index.php?r=ajax/allblogsattraction',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                console.log(query);
                // Query parameters will be ?search=[term]&type=public
                return query;
            }
        }
    });




    $("#tour-chooser").on('select2:select', function (e) {
        var tourName = $("#select2-tour-chooser-container").html();
        var tourId =  $(this).val();
        index += 1;
        
        addRow(tourId, tourName, index);
        
        


    });


});



function deleteRow(e, row){
    e.preventDefault();
    $(row).parent().parent().remove();
    console.log($('#tours-related tbody .item-tour-related').html());
    if(!$('#tours-related tbody .item-tour-related').html()) {
        var tr = `<tr class="item-tour-related">
            <td>
                <input type="hidden" name="AttractionRelated[id][]" value="" />
            </td>
            <td>
            </td>
        </tr>`;

        $('tbody','#tours-related').append(tr);
    }
}


function addRow(id, name, index ){
 
    var tr = `<tr class="item-tour-related">
        <td>
            ${name}
            <input type="hidden" name="AttractionRelated[id][]" value="${id}" />
        </td>
        <td>
            <a href="#" onclick="deleteRow(event, this)">Eliminar</a>
        </td>
    </tr>`;

    $('tbody','#tours-related').append(tr);

    if($('.empty','#tours-related').length > 0){
        $('.empty','#tours-related').remove();
    }
    
}