var indexDestination = $('.item-destination').length;
var indexCountry = $('.item-country').length;
var indexAttraction = $('.item-attraction').length;

$(document).ready(function(){
    $('#add-destination').click(function(e){
        indexDestination += 1;
        addRow($("#destination-table"), $('#destination-name').val(), $('#destination-uri').val(), 'destination', indexDestination);
        e.preventDefault();
    });
    $('#add-country').click(function(e){
        indexCountry += 1;
        addRow($("#country-table"), $('#country-name').val(), $('#country-uri').val(), 'country', indexCountry);
        e.preventDefault();
    });
    $('#add-attraction').click(function(e){
        indexAttraction += 1;
        addRow($("#attraction-table"), $('#attraction-name').val(), $('#attraction-uri').val(), 'attraction', indexAttraction);
        e.preventDefault();
    });
});

var trEmpty = `<tr class="empty">
                    <td colspan="3"> Sin destinos agregados </td>
                </tr>`;

function addRow(target, name, uri,type, index ){

    var tr = `<tr class="item-${type}">
        <td>
            ${name}
            <input type="hidden" name="${type}[${index}][name]" value="${name}" />
        </td>
        <td>
            ${uri}
            <input type="hidden" name="${type}[${index}][value]" value="${uri}" />
        </td>
        <td>
            <a href="#" onclick="deleteRow(event, this)">Eliminar</a>
        </td>
    </tr>`;

    $('tbody',target).append(tr);


    if($('.empty',target).length > 0){
        $('.empty',target).remove();
    }
    
}


function deleteRow(e, row){
    e.preventDefault();
    $(row).parent().parent().remove();
}