var row  = 0;
$(document).ready(function(){
    $("#select-destination").select2({
        width: '510px',
        ajax: {
            url: 'index.php?r=shuttle/ajax-destinations',
            dataType: 'json'
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });

    $("#select-cities").select2({
        width: '510px',
        ajax: {
            url: 'index.php?r=shuttle/ajax-cities',
            dataType: 'json'
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
    });

    $('.btn-add-row').click(function(e){
        e.preventDefault();
        if(($("#select-destination").val() == null) || ($("#select-cities").val() == null)){
            alert('Seleccione ciudad y destino para poder enlazar')
        } else {
            $('.empty-row').remove();
            addRow();
        }
    });


    $('#select-destination').on('select2:select', function (e) {

    });

    $('#select-cities').on('select2:select', function (e) {
        $("#Destination_id_city").val( $(this).val());
        $.ajax({
            url:'index.php?r=shuttle/ajax-zone-city',
            dataType:'json',
            data: {
                id: $(this).val()
            }, 
            success:function(data){;
                $('tbody', '.table-destinations').empty();
                $(data).each(function(index, el){
                    console.log(el);
                    $('tbody', '.table-destinations').append(`
                        <tr>
                            <td>
                                ${el.name} → <strong>${el.zone}</strong>
                            </td>
                            <td>
                                <input type="hidden" name="Destination[${index}][description]" val="${el.desc}" />
                                <input value="${el.id_zone}" type="hidden" name="Destination[${index}][id_zone]" />
                                <input value="${el.id_city}" type="hidden" name="Destination[${index}][id_city]" />
                            </td>   
                            <td>
                                <a href="/index.php?r=shuttle/update&id=${el.id}" ">Editar Contenido</a>
                                <a href="#" onclick="deleteRow(this)">Eliminar</a>
                            </td>
                        </tr>
                    `);
                    row++;
                });

                if(data.length == 0){
                    $('tbody', '.table-destinations').empty();
                    $('tbody', '.table-destinations').append(`
                        <tr class="empty-row">
                            <td colspan=4>
                                <i>Sin destinos enlazados</i>
                            </td>
                        </tr>
                    `);
                }
               
             
            }
        })
    });

});

function deleteRow(target){
    $(target).parent().parent().remove();
}


function addRow() {
    $('tbody', '.table-destinations').append(`
        <tr>
            <td>
                ${$("option:selected","#select-cities").text()} → <strong>${$("option:selected","#select-destination").text()}</strong>
            </td>
            <td>
                <textarea name="Destination[${row}][description]" cols=40></textarea>
                <input type="hidden" value="${$("#select-destination").val()}" name="Destination[${row}][id_zone]" />
                <input type="hidden" value="${$("#select-cities").val()}" name="Destination[${row}][id_city]" />
            </td>   
            <td>
                <a href="#" onclick="deleteRow(this)">Eliminar</a>
            </td>
        </tr>
    `);
}