$(document).ready(function() {
  $("#users-id_user_type").change(function() {
    switch ($(this).val()) {
      case "4":
        $("#section-referred").show();
        $("#users-id_company").val("1");
        $("#users-id_referred").val("");

        $("#section-company").hide();
        $("#section-agency").hide();
        $("#users-id_agency").val("");
        break;
      case "5":
        $("#section-agency").show();
        $("#users-id_company").val("1");
        $("#users-id_referred").val("");

        $("#section-company").hide();
        $("#section-referred").hide();
        break;

      default:
        $("#section-company").show();
        $("#users-id_company").val("");
        $("#users-id_referred").val("");
        $("#users-id_agency").val("");

        $("#section-referred").hide();
        $("#section-agency").hide();
    }
  });
});
