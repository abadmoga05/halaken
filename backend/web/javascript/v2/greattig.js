$(document).ready(function () {
    var hours = new Date().getHours();

    if (hours>=6 && hours<12) {
        var great = 'Buenos días ' +$("#greatting").text();
        $("#greatting").text(great);
        $('.bg-image').attr('style',"background-image: url('/images/morning.jpg');background-position: center");
    }
    else if (hours>=12 && hours<18) {
        var great = 'Buenas tardes ' +$("#greatting").text();
        $("#greatting").text(great);
        $('.bg-image').attr('style',"background-image: url('/images/sunset.jpeg');background-position:center");
    } else {
        var great = 'Buenas noches ' +$("#greatting").text();
        $("#greatting").text(great);
        $('.bg-image').attr('style',"background-image: url('/images/night.jpg');background-position:center");
    }
})