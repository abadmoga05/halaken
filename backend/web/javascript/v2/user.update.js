$(document).ready(function() {
  switch ($("#users-id_user_type").val()) {
    case "4":
      $("#section-referred").show();
      $("#section-company").hide();
      $("#section-agency").hide();
      break;
    case "5":
      $("#section-agency").show();
      $("#section-company").hide();
      $("#section-referred").hide();
      break;

    default:
      $("#section-company").show();
      $("#section-referred").hide();
      $("#section-agency").hide();
  }
});
