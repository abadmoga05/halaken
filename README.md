# yii2

- Project PHP >7  
- yii2 framework  
- MySQL Database


#Config project 

C:\xampp\htdocs\tuexp>composer install

C:\xampp\htdocs\tuexp>init 

Yii Application Initialization Tool v1.0

 

Which environment do you want the application to be initialized in?

 

  [0] Development

  [1] Production

 

  Your choice [0-1, or "q" to quit] 0

 

  Initialize the application under 'Development' environment? [yes|no] yes

#Config VirtualHost

abrir C:\Windows\System32\drivers\etc\host

agregar virtuales

 
# localhost name resolution is handled within DNS itself.

# 127.0.0.1       localhost

# ::1             localhost



127.0.0.1 backend.yii2.com

 

configurar C:\xampp\apache\conf\extra\httpd-vhosts.conf

 

<VirtualHost *:80>

    ServerAdmin admin@yii2.dev

    DocumentRoot "C:/xampp/htdocs/dev.yii2/frontend/web/"

    ServerName backend.yii2.com

    ErrorLog "logs/yii2-error.log"

    CustomLog "logs/devyii2-error.log" common

</VirtualHost>

 

#General config

    #httpd-vhosts.conf considered SO XAMP-WAMP root location
        NameVirtualHost *:80
        <VirtualHost *:80>
        DocumentRoot "C:/xampp/htdocs"
        ServerName localhost
        </VirtualHost>
        <VirtualHost *:80>
            DocumentRoot "C:/xampp/htdocs/lavamap"
            ServerName lavamap.dev
            ServerAlias www.lavamap.dev
            <Directory "C:/xampp/htdocs/lavamap">
                Order allow,deny
                Allow from all
            </Directory>
        </VirtualHost>


    #vhosts (Consider SO)

        127.0.0.1   your.example-domain.dev
        127.0.0.1   backend.yii.com




    #I also uncommented the following line from httpd.conf from Apache
        Include conf/extra/httpd-vhosts.conf

#Consider

- Change in php.ini Apache, use **short_open_tag=On**
- Change in php.ini Apache, use **error_reporting=E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_WARNING & ~E_ERROR** 


 
