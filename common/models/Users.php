<?php

namespace common\models;

use Yii;
use yii\web\IdentityInterface; 

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $email Correo electrónico
 * @property string $password Contraseña
 * @property string $name Nombre
 * @property string $surname Apellidos
 * @property string|null $phone Teléfono
 * @property int|null $active Activo
 * @property int|null $approved Aprovador por un administrador
 * @property int $id_company Empresa
 * @property int $id_user_type
 * @property int|null $id_referred
 * @property int|null $id_agency
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface
{
    const ADMIN = 1;
    const PROVIDER = 2;
    const CUSTOMER = 3;
    const REFERRED = 4;
    const AGENCY = 5;
    const AFFILIATE = 6;
    const CONTEST   = 7;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'name', 'surname', 'id_company', 'id_user_type'], 'required'],
            [['active', 'approved', 'id_company', 'id_user_type', 'id_referred', 'id_agency'], 'integer'],
            [['email', 'name', 'surname', 'phone'], 'string', 'max' => 180],
            [['password'], 'string', 'max' => 240],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'name' => 'Name',
            'surname' => 'Surname',
            'phone' => 'Phone',
            'active' => 'Active',
            'approved' => 'Approved',
            'id_company' => 'Id Company',
            'id_user_type' => 'Id User Type',
            'id_referred' => 'Id Referred',
            'id_agency' => 'Id Agency',
        ];
    }

   

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['id_user_approved' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTours()
    {
        return $this->hasMany(Tour::className(), ['id_user_created' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTours0()
    {
        return $this->hasMany(Tour::className(), ['last_user_updated' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'id_company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(UsersType::className(), ['id' => 'id_user_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferred()
    {
        return $this->hasOne(Referred::className(), ['id' => 'id_referred']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgency()
    {
        return $this->hasOne(Agency::className(), ['id' => 'id_agency']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsAdmin()
    {
        return $this->id_user_type === self::ADMIN;
    }

    public function getIsProvider(){
        return $this->id_user_type === self::PROVIDER;
    }
    
    public function getIsReferred(){
        return $this->id_user_type === self::REFERRED;
    }
    
    public function getIsAgency(){
        return $this->id_user_type === self::AGENCY;
    }

    public function getIsAffiliate(){
        return $this->id_user_type === self::AFFILIATE;
    }

    public function getIsContest(){
        return $this->id_user_type === self::CONTEST;
    }

    /**
     * {@inheritdoc}
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }

    /** 
    * Finds an identity by the given ID. 
    * 
    * @param string|int $id the ID to be looked for 
    * @return IdentityInterface|null the identity object that matches the given ID. 
    */ 
    public static function findIdentity($id) 
    { 
       return static::findOne($id); 
    } 
 
    /** 
    * Finds an identity by the given token. 
    * 
    * @param string $token the token to be looked for 
    * @return IdentityInterface|null the identity object that matches the given token. 
    */ 
   public static function findIdentityByAccessToken($token, $type = null) 
   { 
       return static::findOne(['access_token' => $token]); 
   } 
   /** 
    * Finds an identity by the given token. 
    * 
    * @param string $token the token to be looked for 
    * @return IdentityInterface|null the identity object that matches the given token. 
    */ 
   public static function findEmail($email, $type = null) 
   { 
       return static::findOne(['email' => $email]); 
   } 
 
   /** 
    * @return int|string current user ID 
    */ 
   public function getId() 
   { 
       return $this->id; 
   } 
   
   /** 
    * @return int|string current user ID 
    */ 
    public function getUsername() 
    { 
        return $this->email; 
    } 

   /** 
    * @return string current user auth key 
    */ 
   public function getAuthKey() 
   { 
       return $this->password; 
   } 
 
   /** 
    * @param string $authKey 
    * @return bool if auth key is valid for current user 
    */ 
   public function validateAuthKey($authKey) 
   { 
       return $this->getAuthKey() === $authKey; 
   } 
 
//    public function beforeSave($insert) 
//    { 
//        if (parent::beforeSave($insert)) { 
//            if ($this->isNewRecord) { 
//                $this->auth_key = \Yii::$app->security->generateRandomString(); 
//            } 
//            return true; 
//        } 
//        return false; 
//    } 
 
 
   /** 
    * Validates password 
    * 
    * @param string $password password to validate 
    * @return bool if password provided is valid for current user 
    */ 
   public function validatePassword($password) 
   { 
       $hash = Yii::$app->security->generatePasswordHash($password);
       return Yii::$app->getSecurity()->validatePassword($password, $hash);
   } 


   /**
     * @return boolean
     */
    public function canUpdateRecord()
    {
        if (Yii::$app->user->identity->isAdmin) {
            // Perform custom validation here regardless of "name" attribute value and add error when needed
            if (!Yii::$app->user->identity->isAdmin && isset($this->attributes)) {
                if ($this->attributes["id_user_type"] != 6) {

                if(Yii::$app->user->identity->id_company != $this->attributes["id_company"]){

                        Yii::$app->session->setFlash('error', "La empresa seleccionada no existe");
                        $this->addError('id_company', 'La empresa seleccionada no existe');
                    }
                }

                
            }
        }
    }
 
}
