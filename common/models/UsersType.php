<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users_type".
 *
 * @property int $id
 * @property string $name
 */
class UsersType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * {@inheritdoc}
     * @return UsersTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersTypeQuery(get_called_class());
    }
}
