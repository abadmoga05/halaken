<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Felicidades <?=$lead->name?></title>
  <style type="text/css">
    /* /\/\/\/\/\/\/\/\/ CLIENT-SPECIFIC STYLES /\/\/\/\/\/\/\/\/ */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* /\/\/\/\/\/\/\/\/ RESET STYLES /\/\/\/\/\/\/\/\/ */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}

    /* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

    #bodyCell{padding:20px;}
    #templateContainer{width:600px;}

    /* ========== Page Styles ========== */

    /**
    * @tab Page
    * @section background style
    * @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
    * @theme page
    */
    body, #bodyTable{
      /*@editable*/ background-color:#DEE0E2;
    }

    /**
    * @tab Page
    * @section background style
    * @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
    * @theme page
    */
    #bodyCell{
      /*@editable*/ border-top:4px solid #BBBBBB;
    }

    /**
    * @tab Page
    * @section email border
    * @tip Set the border for your email.
    */
    #templateContainer{
      /*@editable*/ border:1px solid #BBBBBB;
    }

    /**
    * @tab Page
    * @section heading 1
    * @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
    * @style heading 1
    */
    h1{
      /*@editable*/ color:#202020 !important;
      display:block;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:26px;
      /*@editable*/ font-style:normal;
      /*@editable*/ font-weight:bold;
      /*@editable*/ line-height:100%;
      /*@editable*/ letter-spacing:normal;
      margin-top:0;
      margin-right:0;
      margin-bottom:10px;
      margin-left:0;
      /*@editable*/ text-align:left;
    }

    /**
    * @tab Page
    * @section heading 2
    * @tip Set the styling for all second-level headings in your emails.
    * @style heading 2
    */
    h2{
      /*@editable*/ color:#404040 !important;
      display:block;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:20px;
      /*@editable*/ font-style:normal;
      /*@editable*/ font-weight:bold;
      /*@editable*/ line-height:100%;
      /*@editable*/ letter-spacing:normal;
      margin-top:0;
      margin-right:0;
      margin-bottom:10px;
      margin-left:0;
      /*@editable*/ text-align:left;
    }

    /**
    * @tab Page
    * @section heading 3
    * @tip Set the styling for all third-level headings in your emails.
    * @style heading 3
    */
    h3{
      /*@editable*/ color:#606060 !important;
      display:block;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:16px;
      /*@editable*/ font-style:italic;
      /*@editable*/ font-weight:normal;
      /*@editable*/ line-height:100%;
      /*@editable*/ letter-spacing:normal;
      margin-top:0;
      margin-right:0;
      margin-bottom:10px;
      margin-left:0;
      /*@editable*/ text-align:left;
    }

    /**
    * @tab Page
    * @section heading 4
    * @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
    * @style heading 4
    */
    h4{
      /*@editable*/ color:#808080 !important;
      display:block;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:14px;
      /*@editable*/ font-style:italic;
      /*@editable*/ font-weight:normal;
      /*@editable*/ line-height:100%;
      /*@editable*/ letter-spacing:normal;
      margin-top:0;
      margin-right:0;
      margin-bottom:10px;
      margin-left:0;
      /*@editable*/ text-align:left;
    }

    /* ========== Header Styles ========== */

    /**
    * @tab Header
    * @section preheader style
    * @tip Set the background color and bottom border for your email's preheader area.
    * @theme header
    */
    #templatePreheader{
      /*@editable*/ background-color:#F4F4F4;
      /*@editable*/ border-bottom:1px solid #CCCCCC;
    }

    /**
    * @tab Header
    * @section preheader text
    * @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
    */
    .preheaderContent{
      /*@editable*/ color:#808080;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:10px;
      /*@editable*/ line-height:125%;
      /*@editable*/ text-align:left;
    }

    /**
    * @tab Header
    * @section preheader link
    * @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
    */
    .preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */{
      /*@editable*/ color:#606060;
      /*@editable*/ font-weight:normal;
      /*@editable*/ text-decoration:underline;
    }

    /**
    * @tab Header
    * @section header style
    * @tip Set the background color and borders for your email's header area.
    * @theme header
    */
    #templateHeader{
      /*@editable*/ background-color:#F4F4F4;
      /*@editable*/ border-top:1px solid #FFFFFF;
      /*@editable*/ border-bottom:1px solid #CCCCCC;
    }

    /**
    * @tab Header
    * @section header text
    * @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
    */
    .headerContent{
      /*@editable*/ color:#505050;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:20px;
      /*@editable*/ font-weight:bold;
      /*@editable*/ line-height:100%;
      /*@editable*/ padding-top:0;
      /*@editable*/ padding-right:0;
      /*@editable*/ padding-bottom:0;
      /*@editable*/ padding-left:0;
      /*@editable*/ text-align:left;
      /*@editable*/ vertical-align:middle;
    }

    /**
    * @tab Header
    * @section header link
    * @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
    */
    .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
      /*@editable*/ color:#EB4102;
      /*@editable*/ font-weight:normal;
      /*@editable*/ text-decoration:underline;
    }

    #headerImage{
      height:auto;
      max-width:600px;
    }

    /* ========== Body Styles ========== */

    /**
    * @tab Body
    * @section body style
    * @tip Set the background color and borders for your email's body area.
    */
    #templateBody{
      /*@editable*/ background-color:#F4F4F4;
      /*@editable*/ border-top:1px solid #FFFFFF;
      /*@editable*/ border-bottom:1px solid #CCCCCC;
    }

    /**
    * @tab Body
    * @section body text
    * @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
    * @theme main
    */
    .bodyContent{
      /*@editable*/ color:#505050;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:14px;
      /*@editable*/ line-height:150%;
      padding-top:20px;
      padding-right:20px;
      padding-bottom:20px;
      padding-left:20px;
      /*@editable*/ text-align:left;
    }

    /**
    * @tab Body
    * @section body link
    * @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
    */
    .bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{
      /*@editable*/ color:#EB4102;
      /*@editable*/ font-weight:normal;
      /*@editable*/ text-decoration:underline;
    }

    .bodyContent img{
      display:inline;
      height:auto;
      max-width:560px;
    }

    /* ========== Footer Styles ========== */

    /**
    * @tab Footer
    * @section footer style
    * @tip Set the background color and borders for your email's footer area.
    * @theme footer
    */
    #templateFooter{
      /*@editable*/ background-color:#F4F4F4;
      /*@editable*/ border-top:1px solid #FFFFFF;
    }

    /**
    * @tab Footer
    * @section footer text
    * @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
    * @theme footer
    */
    .footerContent{
      /*@editable*/ color:#808080;
      /*@editable*/ font-family:Helvetica;
      /*@editable*/ font-size:10px;
      /*@editable*/ line-height:150%;
      padding-top:20px;
      padding-right:20px;
      padding-bottom:20px;
      padding-left:20px;
      /*@editable*/ text-align:left;
    }

    /**
    * @tab Footer
    * @section footer link
    * @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
    */
    .footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */{
      /*@editable*/ color:#606060;
      /*@editable*/ font-weight:normal;
      /*@editable*/ text-decoration:underline;
    }

    /* /\/\/\/\/\/\/\/\/ MOBILE STYLES /\/\/\/\/\/\/\/\/ */

    @media only screen and (max-width: 480px){
      /* /\/\/\/\/\/\/ CLIENT-SPECIFIC MOBILE STYLES /\/\/\/\/\/\/ */
      body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
      body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */

      /* /\/\/\/\/\/\/ MOBILE RESET STYLES /\/\/\/\/\/\/ */
      #bodyCell{padding:10px !important;}

      /* /\/\/\/\/\/\/ MOBILE TEMPLATE STYLES /\/\/\/\/\/\/ */

      /* ======== Page Styles ======== */

      /**
      * @tab Mobile Styles
      * @section template width
      * @tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn't work for you, set the width to 300px instead.
      */
      #templateContainer{
        max-width:600px !important;
        /*@editable*/ width:100% !important;
      }

      /**
      * @tab Mobile Styles
      * @section heading 1
      * @tip Make the first-level headings larger in size for better readability on small screens.
      */
      h1{
        /*@editable*/ font-size:24px !important;
        /*@editable*/ line-height:100% !important;
      }

      /**
      * @tab Mobile Styles
      * @section heading 2
      * @tip Make the second-level headings larger in size for better readability on small screens.
      */
      h2{
        /*@editable*/ font-size:20px !important;
        /*@editable*/ line-height:100% !important;
      }

      /**
      * @tab Mobile Styles
      * @section heading 3
      * @tip Make the third-level headings larger in size for better readability on small screens.
      */
      h3{
        /*@editable*/ font-size:18px !important;
        /*@editable*/ line-height:100% !important;
      }

      /**
      * @tab Mobile Styles
      * @section heading 4
      * @tip Make the fourth-level headings larger in size for better readability on small screens.
      */
      h4{
        /*@editable*/ font-size:16px !important;
        /*@editable*/ line-height:100% !important;
      }

      /* ======== Header Styles ======== */

      #templatePreheader{display:none !important;} /* Hide the template preheader to save space */

      /**
      * @tab Mobile Styles
      * @section header image
      * @tip Make the main header image fluid for portrait or landscape view adaptability, and set the image's original width as the max-width. If a fluid setting doesn't work, set the image width to half its original size instead.
      */
      #headerImage{
        height:auto !important;
        /*@editable*/ max-width:600px !important;
        /*@editable*/ width:100% !important;
      }

      /**
      * @tab Mobile Styles
      * @section header text
      * @tip Make the header content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
      */
      .headerContent{
        /*@editable*/ font-size:20px !important;
        /*@editable*/ line-height:125% !important;
      }

      /* ======== Body Styles ======== */

      /**
      * @tab Mobile Styles
      * @section body image
      * @tip Make the main body image fluid for portrait or landscape view adaptability, and set the image's original width as the max-width. If a fluid setting doesn't work, set the image width to half its original size instead.
      */
      #bodyImage{
        height:auto !important;
        /*@editable*/ max-width:560px !important;
        /*@editable*/ width:100% !important;
      }

      /**
      * @tab Mobile Styles
      * @section body text
      * @tip Make the body content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
      */
      .bodyContent{
        /*@editable*/ font-size:18px !important;
        /*@editable*/ line-height:125% !important;
      }

      /* ======== Footer Styles ======== */

      /**
      * @tab Mobile Styles
      * @section footer text
      * @tip Make the body content text larger in size for better readability on small screens.
      */
      .footerContent{
        /*@editable*/ font-size:14px !important;
        /*@editable*/ line-height:115% !important;
      }

      .footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */
    }
    /*////// FRAMEWORK STYLES //////*/
    .flexibleContainerCell{padding-top:20px; padding-Right:20px; padding-Left:20px;}
            .flexibleImage{height:auto;}
            .bottomShim{padding-bottom:20px;}
            .imageContent, .imageContentLast{padding-bottom:20px;}
            .nestedContainerCell{padding-top:20px; padding-Right:20px; padding-Left:20px;}
    /*////// MOBILE STYLES //////*/
    @media only screen and (max-width: 480px){          
        /*////// CLIENT-SPECIFIC STYLES //////*/
        body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */

        /*////// FRAMEWORK STYLES //////*/
        /*
            CSS selectors are written in attribute
            selector format to prevent Yahoo Mail
            from rendering media query styles on
            desktop.
        */
        table[id="emailBody"], table[class="flexibleContainer"]{width:100% !important;}

        /*
            The following style rule makes any
            image classed with 'flexibleImage'
            fluid when the query activates.
            Make sure you add an inline max-width
            to those images to prevent them
            from blowing out. 
        */
        img[class="flexibleImage"]{height:auto !important; width:100% !important;}

        /*
            Make buttons in the email span the
            full width of their container, allowing
            for left- or right-handed ease of use.
        */
        table[class="emailButton"]{width:100% !important;}
        td[class="buttonContent"]{padding:0 !important;}
        td[class="buttonContent"] a{padding:15px !important;}

        td[class="textContentLast"], td[class="imageContentLast"]{padding-top:20px !important;}

        /*////// GENERAL STYLES //////*/
        td[id="bodyCell"]{padding-top:10px !important; padding-Right:10px !important; padding-Left:10px !important;}
    }
    .textContent.white{
            color: #fff;
    }
  </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
  <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
      <td align="center" valign="top" id="bodyCell">
        <!-- BEGIN TEMPLATE // -->
        <table border="0" cellpadding="0" cellspacing="0" id="templateContainer">
          <tr>
            <td align="center" valign="top">
              <!-- BEGIN PREHEADER // -->
              <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader">
                <tr>
                  <td valign="top" class="preheaderContent" style="padding-top:10px; padding-right:20px; padding-bottom:10px; padding-left:20px;text-align: center" mc:edit="preheader_content00">
                   &nbsp;
                  </td>
                  <!-- *|END:IF|* -->
                </tr>
              </table>
              <!-- // END PREHEADER -->
            </td>
          </tr>
          <tr>
            <td align="center" valign="top">
              <!-- BEGIN HEADER // -->
              <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader">
                <tr>
                  <td valign="top" class="headerContent">
                    <img src="https://s3.amazonaws.com/cdn2.tuexperiencia.com/email/email-logo.png" style="max-width:600px;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                  </td>
                </tr>
              </table>
              <!-- // END HEADER -->
            </td>
          </tr>
          <tr>
            <td align="center" valign="top">
                <!-- BEGIN BODY // -->

                <? foreach($tours as $book):?>
                <?
                  $tour = $book->tour;
                  $optional = $book->optional;

                  ?>

                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody">
                    <tr>
                        <td valign="top" class="bodyContent" mc:edit="body_content00">
                            <h1 style="color:#dc3545 !important;">CANCELACION</h1>
                            <br>
                            <h1><?=$tour->tourDetails->name?></h1>
                            <h3>Opcional: <?=$optional->name?></h3>
                        </td>
                    </tr>
                    <tr>
                        <td class="bodyContent" style="padding-top:0; padding-bottom:0;">
                            <img src="<?=Yii::$app->params['cdnUrl'].'/'.$tour->cover?>" style="max-width:560px;" id="bodyImage" mc:label="body_image" mc:edit="body_image" mc:allowtext />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="bodyContent" mc:edit="body_content01">
                            <h2 style="color:#dc3545 !important;">Voucher Cancelado: <?=$booking->confirmation?></h2>
                            <h2 style="color:#dc3545 !important;">En Proceso de Cancelación</h2>
                            <h3>Fecha de reserva: <?=\frontend\helpers\FrontUtils::FullDate($book->date_created) ?> </h3>
                            <? if($booking->agency): ?>
                              <h3>Agencia: <?=$booking->agency->comertial_name?></h3>
                            <?endif;?>
                            <table style="width: 100%;">
                                <tr>
                                    <td valign="top" style="width: 50%;">
                                        <h4 style="margin-top:10px; margin-bottom: 5px;">Cliente</h4>
                                        <h3 style="font-style: normal;color:#404040 !important;font-weight: bold; margin-bottom: 5px;"><?=$lead->full_name?></h3>
                                        <h3 style="font-style: normal;color:#404040 !important;font-weight: bold; margin-bottom: 5px;"><?=$lead->phone?></h3>
                                        <h3 style="font-style: normal;color:#404040 !important;font-weight: bold; margin-bottom: 5px;"><?=$lead->email?></h3>
                                        <h4 style="margin-top:10px; margin-bottom: 5px;">Fecha de encuentro</h4>
                                        <h3 style="font-style: normal;color:#404040 !important;font-weight: bold; margin-bottom: 5px;">
                                          <?=strftime("%e de %B de %Y",
                                            DateTime::createFromFormat("Y-m-d",substr($book->travel_date,0,10) )->getTimestamp()); ?>
                                        </h3>
                                        <h4 style="margin-top:10px; margin-bottom: 5px; color:#dc3545 !important;">Fecha de proceso de cancelación</h4>
                                        <h3 style="font-style: normal;color:#404040 !important;font-weight: bold; margin-bottom: 5px;">
                                          <?=strftime("%e de %B de %Y",
                                            DateTime::createFromFormat("Y-m-d", date('Y-m-d'))->getTimestamp()); ?>
                                        </h3>
                                    </td>
                                    <td valign="top"  style="width: 50%;">
                                        <h4 style="margin-top:10px; margin-bottom: 5px;">Desglose</h4>
                                        <table style="width: 100%;">

                                          <? $paxes = $book->passengersTotalAsArrayForSupplier; ?>
                                          <? $total = 0;?>
                                          <? foreach($paxes as $index => $item ):?>
                                              <tr>
                                                  <th> <?=$item['type']?> </th>
                                                  <td <? if( ($index +1 ) == count($paxes)):?>style="border-bottom:1px solid #0a0a0a; "<? endif; ?> >
                                                      $ <?=number_format($item['total'], 2)?> <sup><?=$item['currency']?></sup>
                                                    <? $total += $item['total'];?>
                                                  </td>
                                              </tr>
                                          <? endforeach; ?>

                                          <?
                                          $subtotal = 0;
                                          $coupon = $booking->hasPromoCode;
                                          ?>
                                            <?php if(!$coupon): ?>
                                            <tr>
                                                <td style="text-align: right">
                                                    <h4 style="margin-top:5px; margin-right:35px; margin-bottom: 5px;text-align: right">Total</h4>
                                                </td>
                                                <td>
                                                    $ <?=number_format($total - ($subtotal * -1), 2)?> <sup><?=$item['currency']?></sup>
                                                </td>
                                            </tr>
                                            <?else: ?>
                                            <tr>
                                                <td style="text-align: right">
                                                    <h4 style="margin-top:5px; margin-right:35px; margin-bottom: 5px;text-align: right">Subtotal</h4>
                                                </td>
                                                <td>
                                                    $ <?=number_format($total, 2)?> <sup><?=$item['currency']?></sup>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right">
                                                        <h4 style="margin-top:5px; margin-right:35px; margin-bottom: 5px;text-align: right">Descuento </h4>
                                                </td>
                                                <td>
                                                    <? $discount= $total * ($coupon['discount']/100) ?>
                                                    -$ <?=number_format($discount, 2)?> <sup><?=$item['currency']?></sup>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td style="text-align: right">
                                                        <h4 style="margin-top:5px; margin-right:35px; margin-bottom: 5px;text-align: right"><b>Total</b></h4>
                                                </td>
                                                <td>
                                                    $ <?=number_format($total - $discount, 2)?> <sup><?=$item['currency']?></sup>
                                                </td>
                                            </tr>
                                            <?endif?>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr style="text-align: center">
                                    <td colspan="2">
                                        <h4 style="text-align: center"><?=strip_tags($tour->tourDetails->policy_cancellation)?></h4>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <? endforeach; ?>
                <!-- // END BODY -->
            </td>
          </tr>
            <tr>
            <td align="center" valign="top">
                    <!-- CENTERING TABLE // -->
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" valign="top">
                                <!-- FLEXIBLE CONTAINER // -->
                                <table style="background-color: #bdbebe" border="0" cellpadding="0" cellspacing="0" width="600" class="flexibleContainer">
                                    <tr>
                                        <td>
                                            <div style="text-align: center">
                                            <br />
                                                <h4 style="text-align: center; color:#fff;">¿Necesitas ayuda?</h4>
                                                <span style="text-align: center; color:#fff;">¡Cont&aacute;ctanos!</span>
                                            </div>
                                        </td> 
                                    </tr>
                                    <tr >
                                        <td class="textContent">
                                            <div style="text-align: center">
                                            <br/>
                                            <h4 style="text-align: center; color:#fff;">¿Tienes ya una Reservaci&oacute;n?</h4>
                                            <span  class="data-client">Correo: reservas@tuexperiencia.com</span> <br /><br />
                                            <h4 style="text-align: center; color:#fff;">Atenci&oacute;n a Clientes</h4>
                                            <span class="data-client">Whatsapp: +52 1 55 8028 4011</span>  <br />
                                            <span  class="data-client">Horario: 24/7</span>  <br />
                                            </div>                                  
                                        </td>
                                    </tr>
                                    <tr >
                                        <div style="text-align: center">
                                            <td class="textContent">
                                                <div style="text-align: center">
                                                    <span style="text-align: center; color:#fff;">¡Ll&aacute;manos!</span>
                                                </div>
                                                <div style="text-align: center; font-size: 10px;">
                                                    <span class="data-client">M&Eacute;XICO +52(155) 71000743 - ARGENTINA +54(11) 52390623 - COLOMBIA +57 (1) 3819779</span><br/>
                                                    <span class="data-client">PER&Uacute; +51 (1) 7068483 - NEW YORK +1 (347) 7711893 - MADRID +34 (910) 601813</span>
                                                </div>                                  
                                            </td>
                                        </div>
                                    </tr>
                                </table>
                                <!-- // FLEXIBLE CONTAINER -->
                            </td>
                        </tr>
                    </table>
                    <!-- // CENTERING TABLE -->
                </td>
            </tr>
          <tr>
            <td align="center" valign="top">
              <!-- BEGIN FOOTER // -->
              <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                <tr>
                  <td valign="top" class="footerContent" mc:edit="footer_content00">
                    <a href="https://twitter.com/experiencia_tu">Siguenos en Twitter</a>&nbsp;&nbsp;&nbsp;
                    <a href="https://www.facebook.com/Tuexperiencia-434343833728085/">Danos like en Facebook</a>&nbsp;&nbsp;&nbsp;
                    <a href="https://www.instagram.com/tu_experiencia.viajes/?hl=es-la">Síguenos en instagram</a>&nbsp;
                  </td>
                </tr>
                <tr>
                  <td valign="top" class="footerContent" style="padding-top:0;" mc:edit="footer_content01">
                    <em>Copyright &copy; <?=date('Y')?> tuexperiencia.com Todos los derechos reservados.</em>
                  </td>
                </tr>
              </table>
              <!-- // END FOOTER -->
            </td>
          </tr>
        </table>
        <!-- // END TEMPLATE -->
      </td>
    </tr>
  </table>
</center>
</body>
</html>