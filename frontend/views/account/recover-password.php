<?php
use yii\helpers\Url;
$the_home_url = Url::base(true);
?>
<main>
    <div class="container-fluid mid-section p-0">
        <div class="container">
            <div class="breadcrumb section">
                <span itemscope itemtype="https://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= substr(Yii::$app->params['fullBaseUrlLang'], 0 , -1) ?>">
                            <span itemprop="name"><i class="fa fa-home"></i> Ir al Inicio</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>inicia-sesion">
                            <span itemprop="name" > Mi Cuenta </span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>recuperar-cuenta">
                            <span itemprop="name" style="text-decoration: underline; "><strong> Recuperar Contraseña</strong></span>
                        </a>
                        <meta itemprop="position" content="3" />
                    </span>
                </span>
            </div>
            <br />
            <div class="commn_hdr pd-45 text-center wow fadeInUp">
                <h1>Recuperar Contraseña</h1>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="payment-outr">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12">
                                <div class="payment-info" style="background: #ebebeb;padding: 10px 22px 10px 84px;border: 2px solid #ddd;position: relative;">
                                    <ul>
                                        <li><strong>Recuperar Contraseña</strong></li>
                                    </ul>
                                </div>
                                <div class="create-account-innr">
                                    <h3>&nbsp;</h3>
                                    <div class="success-message"></div>
                                    <form class="data-book form-recover-account needs-validation" method="POST" action="<?= Yii::$app->params['fullBaseUrlLang']?>send-recover-email" novalidate>
                                        <?php if ($info): ?>
                                        <div class="lead-error">
                                            <div class="alert alert-info"><?= $info ?></div>
                                        </div>
                                        <?php else: ?>
                                        <div class="alert alert-success">Por favor, ingrese el correo electrónico usado al registrarte. </div>
                                        <div class="row">
                                            <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                                <div class="form-innr">
                                                    <input class="form-control" id="recover_email" name="recover_email" type="email"
                                                        placeholder="Correo electr&oacute;nico *" required/>
                                                    <div class="invalid-feedback">
                                                        Falta Email!
                                                    </div>
                                                    <input  type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:10px 0px;">
                                            <div class="col-lg-offset-2 col-lg-6 col-sm-offset-2 col-sm-6 col-xs-12">
                                                <div class="form-innr">
                                                <input type="button" id="recuperar" name="recuperar" class="btn btn-green" style="display: block;margin-top: 30px;background:#0aaa9e;color:#ffffff" value="Recuperar mi cuenta"/>    
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif;?>                                    
                                    </form>
                                </div>


                            </div>
                        </div>              
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--  END SECTION  -->