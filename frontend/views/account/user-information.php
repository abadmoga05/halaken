<?php 
    $baseUrl = Yii::$app->params['urlManagerFrontEnd'];
?>

<main>
    <div class="container-fluid mid-section p-0">
        <div class="container">
            <div class="breadcrumb section">
                <span itemscope itemtype="https://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= substr(Yii::$app->params['fullBaseUrlLang'], 0 , -1) ?>">
                            <span itemprop="name"><i class="fa fa-home"></i> Ir al Inicio</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta">
                            <span itemprop="name" style="text-decoration: underline; "><strong> Mis Experiencias</strong></span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </span> 
                </span>
            </div>
            <br />
            <div class="payment-outr">
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        <div class="payment-info">
                            <ul>
                                <li><strong>Mi Cuenta</strong></li>
                            </ul>
                        </div>
                        <div class="summary-outr">
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang']?>mi-cuenta">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-map-marker"></i></span>
                                        Mis Experiencias
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang']?>mi-cuenta/actualizar-datos">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-user"></i></span>
                                        Actualizar mi información
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang']?>mi-cuenta/cambiar-acceso">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-key"></i></span>
                                        Cambiar contraseña
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang']?>mi-cuenta/cerrar-sesion">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-power-off"></i></span>
                                        Cerrar Sesion
                                    </a>
                                </div>
                            </div>                        
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8">
                        <div class="point-error"></div>
                        <div class="payment-info">
                            <ul>
                                <li>
                                    <strong>Mis Reservaciones</strong>
                                    <?php if ($total_pages > 1 && $page > 0): ?>
                                    <strong> - Página <?= $page ?> </strong>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" for="tab">
                            <li class="<?= $type == 'current' || empty($type)? 'active' : '' ?>">
                                <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta?type=current" role="tab">
                                    <i class="fa fa-envelope"></i>Actividades por realizar
                                </a>
                            </li>
                            <li class="<?= $type == 'history' ? 'active' : '' ?>">
                                <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta?type=history" role="tab">
                                    <i class="fa fa-cog"></i>  Actividades anteriores
                                </a>
                            </li>
                            <li class="<?= $type == 'canceled' ? 'active' : '' ?>">
                                <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta?type=canceled" role="tab">
                                    <i class="fa fa-cog"></i>  Canceladas
                                </a>
                            </li>
                        </ul>
                            
                        <!-- Tab panes -->                    
                        <?php if (empty($bookings) || sizeof($bookings) == 0): ?>
                            <?php if ($type && $type == 'canceled'): ?>
                            <div class="alert alert-info">Aun no tienes reservaciones canceladas.</div>
                            <?php elseif ($type && $type == 'history'): ?>
                            <div class="alert alert-info">Aun no tienes un historial de reservaciones pasadas.</div>
                            <?php elseif (empty($type) || $type == 'current'): ?>
                            <div class="alert alert-info">No tienes reservaciones, te invitamos a reservar alguna de nuestras experiencias.</div>
                            <?php endif;?>
                        <?php else: ?>
                            <br>
                            <?php foreach ($bookings as $booking): ?>
                            <div class="row">
                                <div class="col-lg-12 col-sm-12">
                                <?php if (property_exists($booking, 'item_list')): ?>
                                    <?php foreach ($booking->item_list as $item): ?>
                                    <div class="col-12 exp_list_outr table-responsive" id="tours-list">
                                        <div class="card dest-box mt-4">
                                            <div class="row no-gutters">
                                                <div class="col-md-4 border-right">
                                                    <img class="img-fluid lazy" alt="<?= $item->name ?>" rel="nofollow" src="<?= Yii::$app->params['cdnUrl'] . '/' . $item->image ?>" style="">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card-block dest-details px-2">
                                                        <div class="card-title heading mb-0"><?= $item->name ?></div>
                                                        <p class="card-text text-justify"><?= $item->description ?></p>
                                                        <?php if ($item->status==3): ?>
                                                        <p class="card-text text-center">CANCELADO</p>
                                                        <?php endif; ?>
                                                        <?php if ($item->descuento): ?>
                                                        <div class="card-text row no-gutters border-bottom pb-3 mb-2">
                                                            <div class="col p-0">
                                                                <div class="text-light text-center text-uppercase rounded pr-2 pl-2  " style="background: #00a395">
                                                                <?= $item->descDescuento ?>
                                                                </div>
                                                            </div>
                                                            <div class="col text-center">
                                                                <div class="dest-duration"><?= $item->symbol . ' ' . $item->descuento . ' ' . $item->currency?></div>
                                                            </div>
                                                        </div>
                                                        <?php endif; ?>
                                                        <div class="card-text row no-gutters border-bottom pb-3 mb-2">
                                                            <div class="col p-0">
                                                                <div class="text-light text-center text-uppercase rounded pr-2 pl-2  " style="<?= $item->status==3 ? 'background: #dc3545' : 'background: #00a395' ?>">
                                                                Folio
                                                                </div>
                                                            </div>
                                                            <div class="col text-center">
                                                                <div class="dest-duration"><?= $item->confirmation ?></div>
                                                            </div>
                                                        </div>
                                                        <div class="card-text row no-gutters border-bottom pb-3 mb-2">
                                                            <div class="col p-0">
                                                                <div class="text-light text-center text-uppercase rounded pr-2 pl-2  " style="<?= $item->status==3 ? 'background: #dc3545' : 'background: #00a395' ?>">
                                                                Fecha
                                                                </div>
                                                            </div>
                                                            <div class="col text-center">
                                                                <div class="dest-duration"><?= $item->travel_date ?></div>
                                                            </div>
                                                        </div>
                                                        <div class="card-text row no-gutters border-bottom pb-3 mb-2">
                                                            <div class="col p-0">
                                                                <div class="text-light text-center text-uppercase rounded pr-2 pl-2  " style="<?= $item->status==3 ? 'background: #dc3545' : 'background: #00a395' ?>">
                                                                Cantidad
                                                                </div>
                                                            </div>
                                                            <div class="col text-center">
                                                                <div class="dest-duration"><?= $item->quantity ?></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer dest-details w-100 text-muted m-0 bg-white">
                                                <div class="row">
                                                    <?php if ($item->descuento): ?>
                                                    <div class="col p-0">
                                                        <p><label class="dest-discounted "><strong>Subtotal:  </strong></label><span class="dest-discounted "><?= $item->symbol . ' ' . $item->price . ' ' . $item->currency?></span></p>
                                                        <p><label class="dest-discounted "><strong>Descuento:-  </strong></label><span class="dest-discounted "><?= $item->symbol . ' ' . $item->descuento . ' ' . $item->currency?></span></p>
                                                        <p><label class="dest-discounted "><strong>Total:  </strong></label><span class="dest-discounted "><?= $item->symbol . ' ' . $item->totalConDesc . ' ' . $item->currency?></span></p>
                                                    </div>
                                                    <?php else: ?>
                                                    <div class="col p-0">
                                                        <p><label class="dest-discounted "><strong>Total:  </strong></label><span class="dest-discounted "><?= $item->symbol . ' ' . $item->price . ' ' . $item->currency?></span></p>
                                                    </div>
                                                    <?php endif;?>
                                                    
                                                    <div class="col p-0 text-right">
                                                        <p><a class="btn btn-green" style="background-color: #f4733d;color: #fff;" href="/confirmacion/<?= $item->confirmation ?>" target="_blank"><span><i class="fa fa-download"></i></span> Descargar</a>
                                                        <input type="hidden" name="confirma" id="confirma" value="<?= $item->confirmation ?>"></p>
                                                        <?php if ($item->valida): ?>
                                                        <button type="button" class="btn btn-danger" id="show-cancel-tour"  data-id="<?= $item->confirmation ?>" data-tourname="<?= $item->name ?>" data-toggle="modal" data-target="#cancelTourModal"><span><i class="fa fa-trash"></i> Cancelar</span></button>
                                                        <?php endif;?>
                                                    </div>
                                                </div>
                                             
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                <div class="">
                                            <h2>
                                                Actividad no disponible
                                            </h2>
                                            <div class="activity_time_sec has-ribbon">
                                                <div class="activity_ofr">
                                                    <p>
                                                        <span>Cantidad: </span>
                                                    </p>
                                                </div>
                                                <div class="lst_time">
                                                    <p>No disponible</p>
                                                </div>
                                            </div>
                                            <div class="activity_time_sec has-ribbon">
                                                <div class="activity_ofr">
                                                    <p>
                                                        <span>SKU: </span>
                                                    </p>
                                                </div>
                                                <div class="lst_time">
                                                    <p>No disponible</p>
                                                </div>

                                            </div>

                                            <div class="activity_time_sec has-ribbon">
                                                <div class="activity_ofr">
                                                    <p>
                                                        <span>Fecha: </span>
                                                    </p>
                                                </div>
                                                <div class="lst_time">
                                                <p><?= $booking->travel_date ?></p>
                                                </div>
                                            </div>
                                            <div class="activity_time_sec has-ribbon">
                                                <div class="activity_ofr">
                                                    <p>
                                                        <span>Confirmación: </span>
                                                    </p>
                                                </div>
                                                <div class="lst_time">
                                                    <a class="btn btn-green" href="/confirmacion/<?= $booking->confirmation ?>">Abrir</a>
                                                </div>

                                            </div>
                                        </div>                                
                                <?php endif; ?>
                                </div>
                            </div>
                            <h3>&nbsp;</h3>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if ($total_pages > 1): ?>
                            <ul class="pagination justify-content-center">
                                <!-- Link of the first page -->
                                <li class='page-item <?= ($page <= 1 ? 'disabled' : '')?>'>
                                    <a class='page-link' href='/mi-cuenta?page=1<?= !empty($type) ? '&type=' . $type : '' ?>'><<</a>
                                </li>
                                <!-- Link of the previous page -->
                                <li class='page-item <?= ($page <= 1 ? 'disabled' : '')?>'>
                                    <a class='page-link' href='/mi-cuenta?page=<?= ($page>1 ? $page-1 : 1)?><?= !empty($type) ? '&type=' . $type : '' ?>'><</a>
                                </li>
                                <!-- Links of the pages with page number -->
                                <?php for ($i=$start; $i<=$end; $i++): ?>
                                <li class='page-item <?= ($i == $page ? 'active' : '')?>'>
                                    <a class='page-link' href='/mi-cuenta?page=<?= $i;?><?= !empty($type) ? '&type=' . $type : '' ?>'><?= $i;?></a>
                                </li>
                                <?php endfor; ?>
                                <!-- Link of the next page -->
                                <li class='page-item <?= ($page >= $total_pages ? 'disabled' : '')?>'>
                                    <a class='page-link' href='/mi-cuenta?page=<?= ($page < $total_pages ? $page+1 : $total_pages)?><?= !empty($type) ? '&type=' . $type : '' ?>'>></a>
                                </li>
                                <!-- Link of the last page -->
                                <li class='page-item <?= ($page >= $total_pages ? 'disabled' : '')?>'>
                                    <a class='page-link' href='/mi-cuenta?page=<?= $total_pages;?><?= !empty($type) ? '&type=' . $type : '' ?>'>>></a>
                                </li>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>                    
        </div>
    </div>
</main>

<div class="modal fade" id="cancelTourModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form class="data-book cancela-reserva" method="POST" action="<?= Yii::$app->params['fullBaseUrlLang']?>mi-cuenta/update-tour-cancel">
                <div class="modal-header">
                    <h4 class="modal-title">Cancelar Reservación<span id="tour-name"></span></h4>
                </div>
                <div class="modal-body">
                    <p>Al cancelar el tour, estas de acuerdo con nuestra política de cancelación, 
                    la cual puedes leer mediante el siguiente enlace:</p>
                    <p><a href="/politica-de-cancelacion" target="_blank">Política de cancelación</a></p>
                    <p>Esta acción no se puede deshacer</p>
                    <p>Cancelar: </p>
                    <input type="text" class="form-control" id="recipient-name" name="recipient-name" readonly>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="cancela" name="cancela" class="btn btn-danger delBtn">Cancelar</button>
                </div>
                <input type="hidden" name="confirmacion" id="confirmacion" />
                <input type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                                    value="<?=Yii::$app->request->getCsrfToken()?>" />
            </form>
        </div>
    </div>
</div>
<!--  END SECTION  -->
