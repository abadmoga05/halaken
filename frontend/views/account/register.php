<?php
use yii\helpers\Url;
$the_home_url = Url::base(true);
?>

<main>
    <div class="container-fluid mid-section p-0">
        <div class="container">
            <div class="breadcrumb section">
                <span itemscope itemtype="https://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= substr(Yii::$app->params['fullBaseUrlLang'], 0 , -1) ?>">
                            <span itemprop="name"><i class="fa fa-home"></i> Ir al Inicio</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>inicia-sesion">
                            <span itemprop="name" > Mi Cuenta </span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>registrarse">
                            <span itemprop="name" style="text-decoration: underline; "><strong> Registrarme</strong></span>
                        </a>
                        <meta itemprop="position" content="3" />
                    </span> 
                </span>
            </div>
            <br />
            <div class="commn_hdr pd-45 text-center wow fadeInUp">
                <h1>Registrarme</h1>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12">
                    <div class="payment-outr">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12">
                                <div class="payment-info" style="background: #ebebeb;padding: 10px 22px 10px 84px;border: 2px solid #ddd;position: relative;">
                                    <ul>
                                        <li><strong>Registrarme</strong></li>
                                    </ul>
                                </div>
                                <?php if ($error): ?>
                                <div class="lead-error">
                                    <div class="alert alert-danger"><?= $error ?></div>
                                </div>
                                <?php endif; ?> 
                                <?php if ($info): ?>
                                <div class="lead-error">
                                    <div class="alert alert-info"><?= $info ?></div>
                                </div>
                                <?php else: ?>
                                <div class="create-account-innr" align="center">
                                    <h4>&nbsp;</h4>
                                    <form class="data-book form-register needs-validation" method="POST" action="<?= Yii::$app->params['fullBaseUrlLang']?>send-registration" novalidate>
                                        <div class="lead-error"></div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                                <div class="form-innr">
                                                    <label for="name" style="display:none;">Nombre</label>
                                                    <input class="form-control" id="name" name="name" type="text"
                                                        placeholder="Nombre(s) *" required/>
                                                        <div class="invalid-feedback">
                                                            Falta Nombre!
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2  col-lg-8 col-sm-offset-2 col-sm-8 col-xs-6">
                                                <div class="form-innr">
                                                    <label for="surname" style="display:none;">Apellido (s)</label>
                                                    <input class="form-control" id="last_name" name="last_name" type="text"
                                                        placeholder="Apellido (s) *" required/>
                                                        <div class="invalid-feedback">
                                                            Faltan Apellidos!
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                                <div class="form-innr">
                                                    <label for="email" style="display:none;">E-mail</label>
                                                    <input class="form-control" id="email" name="email" type="email"
                                                        placeholder="Correo electr&oacute;nico *" required/>
                                                        <div class="invalid-feedback">
                                                            Falta Email!
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                                <div class="form-innr phone_number_class">
                                                    <label for="phone_number" style="display:none;">Télefono</label>
                                                    <input class="form-control" type="tel" name="phone_number" id="phone_number" value="" required>
                                                    <span id="error-msg" class="hide"></span>
                                                    <div class="invalid-feedback">
                                                            Falta Telefono!
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8 col-xs-12">
                                                <div class="form-innr">
                                                    <label for="password" style="display:none;">Contraseña</label>
                                                    <input class="form-control" id="password" name="password" type="password"
                                                        placeholder="Contraseña *" required/>
                                                        <div class="invalid-feedback">
                                                            Falta Contraseña!
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8 col-xs-12">
                                                <div class="form-innr">
                                                    <label for="confirm_password" style="display:none;">Confirmar Contraseña</label>
                                                    <input class="form-control" id="confirm_password" name="confirm_password" type="password"
                                                        placeholder="Confirmar Contraseña *" required/>
                                                        <div class="invalid-feedback">
                                                            Falta Confirmar Contraseña!
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div style="margin:15px 0px" class="col-lg-offset-2 col-lg-4 col-sm-offset-2 col-sm-4 col-xs-12">
                                                <div class="form-innr">
                                                    <button id="register" name="register" class="resrv-btn quote-button btn btn-green" >Registarme</button>
                                                </div>
                                            </div>
                                            <div style="margin:15px 0px" class="col-lg-4 col-sm-4 col-xs-12">
                                                <div class="form-innr">
                                                    <a href="<?= Yii::$app->params['fullBaseUrlLang']?>inicia-sesion" class="btn btn-green" >Iniciar Sesión</a>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->getCsrfToken()?>" />                              
                                    </form>
                                </div>
                                <?php endif;?>
                            </div>
                        </div>           
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--  END SECTION  -->