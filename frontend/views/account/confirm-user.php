<!--  PAYMENT SECTION -->


<section class="payment-sec pd-50" id="mouse">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-8 col-sm-12">
                <div class="payment-outr">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <div class="payment-info">
                                <ul>
                                    <li><strong>Confirmación de Cuenta</strong></li>
                                </ul>
                            </div>
                            <div class="create-account-innr">
                                <h3>&nbsp;</h3>
                                <form class="data-book form-confirm-user">
                                    <div class="alert alert-success">Se ha envíado un código de verificación a tu correo electrónico de registro. Por favor ingresalo aquí</div>                                
                                    <div class="lead-error"></div>
                                    <div class="row">
                                        <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                            <div class="form-innr">
                                                <input id="verify_code" name="code" type="text"
                                                    placeholder="Código de verificación *" autocomplete="off" />
                                                <input type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->getCsrfToken()?>" />                              
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-offset-2 col-lg-4 col-sm-offset-2 col-sm-4 col-xs-12">
                                            <div class="form-innr">
                                                <button class="date-btn resrv-btn quote-button" style="display: block;margin-top: 30px;">Verificar Cuenta</button>
                                            </div>
                                        </div>
                                    </div>                                    
                                </form>
                            </div>


                        </div>
                    </div>               
                </div>
            </div>
        </div>
    </div>
</section>
<!--  END SECTION  -->