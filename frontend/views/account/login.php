<?php
use yii\helpers\Url;
use yii\widgets\MaskedInput;
$the_home_url = Url::base(true);
?>

<main>
    
    <div class="container-fluid mid-section p-0">
        <div class="container">
            <div class="breadcrumb section">
                <span itemscope itemtype="https://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= substr(Yii::$app->params['fullBaseUrlLang'], 0 , -1) ?>">
                            <span itemprop="name"><i class="fa fa-home"></i> Ir al Inicio</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>inicia-sesion">
                            <span itemprop="name" style="text-decoration: underline; "><strong> Mi Cuenta</strong></span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </span>
                </span>
            </div>
            <br />
            <div class="commn_hdr pd-45 text-center wow fadeInUp">
                <h1>Mi Cuenta</h1>
            </div>
            <div class="container pt-5 pb-3 cart-container">
                <div class=" col-md-12 col-sm-12">
                    <div class="payment-outr">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12">
                                <div class="payment-info" style="background: #ebebeb;padding: 10px 22px 10px 84px;border: 2px solid #ddd;position: relative;">
                                    <ul>
                                        <li><strong>Inicia Sesión</strong></li>
                                    </ul>
                                </div>
                                <div>
                                    <h4>&nbsp;</h4>
                                    <form class="data-book form-login needs-validation"  method="POST" action="<?= Yii::$app->params['fullBaseUrlLang']?>send-login" novalidate>
                                        <div class="lead-error"></div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                                <div class="form-innr">
                                                    <label for="email" style="display:none;">E-mail</label>
                                                    <input class="form-control" id="login_email" name="email" type="email"
                                                        placeholder="Email *" required/>
                                                        <div class="invalid-feedback">
                                                            Falta Email!
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8 col-xs-12">
                                                <div class="form-innr">
                                                    <label for="password" style="display:none;">Contraseña</label>
                                                    <input class="form-control" id="password" name="password" type="password"
                                                        placeholder="Contraseña *" required/>
                                                    <div class="invalid-feedback">
                                                        Falta Contraseña!
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8 col-xs-12">
                                                <div class="form-innr">
                                                    <label for="password" style="display:none;">Confirma Contraseña</label>
                                                    <input class="form-control" id="confirm_password" name="confirm_password" type="password"
                                                        placeholder="Confirma Contraseña *" required/>
                                                    <div class="invalid-feedback">
                                                        Falta Confirmar Contraseña!
                                                    </div>
                                                    <input type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->getCsrfToken()?>" /> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8 col-xs-12">
                                                <div class="form-innr">
                                                    <a href="<?= Yii::$app->params['fullBaseUrlLang']?>recuperar-cuenta" style="display: block;margin-top: 30px;">Olvide mi contraseña</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin:15px 0px">
                                            <div style="margin:15px 0px" class="col-lg-offset-2 col-lg-4 col-sm-offset-2 col-sm-4 col-xs-12">
                                                <div class="form-innr">
                                                    <input type="button" id="login" name="login" class="btn btn-green resrv-btn quote-button" value="Iniciar Sesión"/>
                                                </div>
                                            </div>
                                            <div style="margin:15px 0px" class="col-lg-4 col-sm-4 col-xs-12">
                                                <div class="form-innr">
                                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>registrarse" class="btn btn-green">Registrarme</a>
                                                </div>
                                            </div>
                                        </div>                                    
                                    </form>
                                </div>
                            </div>
                        </div>           
                    </div>
                </div>
            </div>
        </div>
    </div>
    </main>
<!--  END SECTION  -->