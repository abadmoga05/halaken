<?php 
    $urlFront = Yii::$app->params['urlManagerFrontEnd'];
?>

<main>
    <div class="container-fluid mid-section p-0">
        <div class="container">
            <div class="breadcrumb section">
                <span itemscope itemtype="https://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= substr(Yii::$app->params['fullBaseUrlLang'], 0 , -1) ?>">
                            <span itemprop="name"><i class="fa fa-home"></i> Ir al Inicio</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/caambiar-acceso">
                            <span itemprop="name" style="text-decoration: underline; "><strong> Cambiar Acceso</strong></span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </span> 
                </span>
            </div>
            <br />
            <div class="payment-outr">
                <div class="row">
                    <div class="col-lg-4 col-sm-4">

                        <div class="payment-info">
                            <ul>
                                <li><strong>Mi Cuenta</strong></li>
                            </ul>
                        </div>
                        <div class="summary-outr">
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-map-marker"></i></span>
                                        Mis Experiencias
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/actualizar-datos">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-user"></i></span>
                                        Actualizar mi información
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/cambiar-acceso">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-key"></i></span>
                                        Cambiar contraseña
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/cerrar-sesion">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-key"></i></span>
                                        Cerrar Sesion
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-8 col-sm-8">

                        <div class="point-error"></div>

                        <div class="payment-info">
                            <ul>
                                <li><strong>Cambiar contraseña</strong></li>
                            </ul>
                        </div>
                        <div class="create-account-innr">
                            <h3>&nbsp;</h3>
                            <form class="data-book form-user-change-password needs-validation" method="POST" action="<?= Yii::$app->params['fullBaseUrlLang']?>update-user-change-password" novalidate>
                                <?php if ($error):?>
                                <div class="lead-error">
                                    <div class="alert alert-danger"><?= $error['message'] ?></div>
                                </div>
                                <?php else: ?>
                                <div></div>
                                <div class="alert alert-info">Al iniciar el cambio de contraseña, se desactivara la sesión actual por seguridad de la cuenta.</div>
                                <div class="lead-error"></div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-6 col-sm-offset-2 col-sm-6 col-xs-12">
                                        <div class="form-innr">
                                            <input type="hidden" name="<?=Yii::$app->request->csrfParam?>" 
                                                value="<?=Yii::$app->request->getCsrfToken()?>" />
                                                <input type="button" id="cambiar-passsword" name="cambiar-passsword" class="btn btn-green" style="display: block;margin-top: 30px;background:#0aaa9e;color:#ffffff" value="Cambiar contraseña"/>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="row"><br>&nbsp;</div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!--  END SECTION  -->