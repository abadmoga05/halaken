
<main>
    <div class="container-fluid mid-section p-0">
        <div class="container">
            <div class="col-lg-12 col-sm-12">
                <div class="payment-outr">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <div class="payment-info" style="background: #ebebeb;padding: 10px 22px 10px 84px;border: 2px solid #ddd;position: relative;margin:10px 0px">
                                <ul>
                                    <li><strong>Cambiar contraseña</strong></li>
                                </ul>
                            </div>
                            <div class="create-account-innr">
                                <h3>&nbsp;</h3>
                                <form class="data-book form-change-password needs-validation" method="POST" action="<?= Yii::$app->params['fullBaseUrlLang']?>send-change-password" novalidate>
                                    <?php if ($error): ?>
                                    <div class="lead-error">
                                        <div class="alert alert-danger"><?= $error ?></div>
                                    </div>
                                    <?php endif; ?> 
                                    <?php if ($info): ?>
                                    <div class="lead-error">
                                        <div class="alert alert-info"><?= $info ?></div>
                                    </div>
                                    <div style="margin:15px 0px" class="col-lg-4 col-sm-4 col-xs-12">
                                        <div class="form-innr">
                                            <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>inicia-sesion" class="btn btn-green">Login</a>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                    <div class="alert alert-info">Por favor, ingresa aquí el código que te hemos envíado a tu correo electronico de la cuenta.</div>
                                    <div class="row" style="margin:10px 0px;">
                                        <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                            <div class="form-innr">
                                                <input class="form-control" id="recover_code" name="recover_code"
                                                    placeholder="Clave de restablecimiento de contraseña *" autocomplete="off" required/>
                                                <div class="invalid-feedback">
                                                    Falta Clave!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin:10px 0px;">
                                        <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                            <div class="form-innr">
                                                <input class="form-control" id="password" name="password" type="password"
                                                    placeholder="Nueva contraseña *" required/>
                                                <div class="invalid-feedback">
                                                    Falta Contraseña!
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin:10px 0px;">
                                        <div class="col-lg-offset-2 col-lg-8 col-sm-offset-2 col-sm-8 col-xs-12">
                                            <div class="form-innr">
                                                <input class="form-control" id="confirm_password" name="confirm_password" type="password"
                                                    placeholder="Confirmar contraseña *" required/>
                                                <div class="invalid-feedback">
                                                    Falta Confirmar Contraseña!
                                                </div>
                                                <input type="hidden" id="id" name="id" value="<?=$dataUser['id']?>"/>
                                                <input type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->getCsrfToken()?>" />                                                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin:10px 0px;">
                                        <div class="col-lg-offset-2 col-lg-6 col-sm-offset-2 col-sm-6 col-xs-12">
                                            <div class="form-innr">
                                                <input type="button" id="restablece-passsword" name="restablece-passsword" class="btn btn-green" style="display: block;margin-top: 30px;background:#0aaa9e;color:#ffffff" value="Cambiar contraseña"/>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="row"><br>&nbsp;</div>
                                </form>
                            </div>
                        </div>
                    </div>             
                </div>
            </div>
        </div>
    </div>
</main>
<!--  END SECTION  -->