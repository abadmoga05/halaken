
<main>
    <div class="container-fluid mid-section p-0">
        <div class="container">
            <div class="breadcrumb section">
                <span itemscope itemtype="https://schema.org/BreadcrumbList">
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= substr(Yii::$app->params['fullBaseUrlLang'], 0 , -1) ?>">
                            <span itemprop="name"><i class="fa fa-home"></i> Ir al Inicio</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </span>
                    <span class="slash">/</span>
                    <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/actualizar-datos">
                            <span itemprop="name" style="text-decoration: underline; "><strong> Actualizar Datos</strong></span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </span> 
                </span>
            </div>
            <br />
            <div class="payment-outr">
                <div class="row">
                    <div class="col-lg-4 col-sm-4">
                        <div class="payment-info">
                            <ul>
                                <li><strong>Mi Cuenta</strong></li>
                            </ul>
                        </div>
                        <div class="summary-outr">
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-map-marker"></i></span>
                                        Mis Experiencias
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/actualizar-datos">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-user"></i></span>
                                        Actualizar mi información
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/cambiar-acceso">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-key"></i></span>
                                        Cambiar contraseña
                                    </a>
                                </div>
                            </div>
                            <div class="client_slide" style="">
                                <div class="client-txt">
                                    <a href="<?= Yii::$app->params['fullBaseUrlLang'] ?>mi-cuenta/cerrar-sesion">
                                        <span style="color: #fff;font-size: 20px;background: #0aaa9e;margin-left: 10px;"
                                            class="badge"><i class="fa fa-key"></i></span>
                                        Cerrar Sesion
                                    </a>
                                </div>
                            </div>                        
                        </div>
                    </div>
                    <div class="col-lg-8 col-sm-8">
                        <div class="point-error"></div>
                        <div class="payment-info">
                            <ul>
                                <li><strong>Actualizar mi información</strong></li>
                            </ul>
                        </div>
                        <div class="create-account-innr">
                            <h3>&nbsp;</h3>
                            <?php if ($error): ?>
                            <div class="lead-error">
                                <div class="alert alert-danger"><?= $error ?></div>
                            </div>
                            <?php endif; ?> 
                            <?php if ($info): ?>
                            <div class="lead-error">
                                <div class="alert alert-info"><?= $info ?></div>
                            </div>
                            <?php else: ?>                       
                            <form class="data-book form-update-info needs-validation" method="POST" action="<?= Yii::$app->params['fullBaseUrlLang']?>update-user-information" novalidate>
                                <div class="lead-error"></div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                        <div class="form-innr" style="margin:10px 0px">
                                            <input  class="form-control" id="name" name="name" type="text" placeholder="Nombre(s) *" value="<?= $userData['name'] ?>" required/>
                                            <div class="invalid-feedback">
                                                Falta Nombre!
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-offset-2  col-lg-8 col-sm-offset-2 col-sm-8 col-xs-6">
                                        <div class="form-innr" style="margin:10px 0px">
                                            <input  class="form-control" id="last_name" name="last_name" type="text"
                                                placeholder="Apellido (s) *" value="<?= $userData['last_name'] ?>" required/>
                                            <div class="invalid-feedback">
                                                Faltan Apellidos!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                        <div class="form-innr" style="margin:10px 0px">
                                            <input  class="form-control" id="email" name="email" type="email"
                                                placeholder="Correo electr&oacute;nico *" disabled value="<?= $userData['email'] ?>" required/>
                                            <div class="invalid-feedback">
                                                Falta Email!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-8  col-sm-offset-2 col-sm-8 col-xs-6">
                                        <div class="form-innr" style="margin:10px 0px">
                                            <input  class="form-control" id="phone_number" name="phone_number" value="<?= $userData['phone'] ?>" required/> 
                                            <div class="invalid-feedback">
                                                Falta Teléfono!
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-4 col-sm-offset-2 col-sm-4 col-xs-12">
                                        <div class="form-innr" style="margin:10px 0px">
                                            <input type="button" id="actualiza" name="actualiza" class="btn btn-green" style="display: block;margin-top: 30px;background:#0aaa9e;color:#ffffff" value="Actualizar"/>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="<?=$userData['id']?>" />
                                <input type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                                    value="<?=Yii::$app->request->getCsrfToken()?>" />
                            </form>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--  END SECTION  -->