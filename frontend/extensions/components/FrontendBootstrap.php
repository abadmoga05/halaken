<?
namespace frontend\extensions\components;

use yii\base\Application;
use yii\base\BootstrapInterface;
use Yii;


class FrontendBootstrap implements BootstrapInterface
{
    /**
    * Bootstrap method to be called during application bootstrap stage.
    * @param Application $app the application currently running
    */
    public function bootstrap($app)
    {

        $routes = Yii::$app->params['routes'];
        $countries = Yii::$app->params['countries'];
        
        $parts = explode('/', $_SERVER['REQUEST_URI']);
        $country = "";
        if(in_array($parts[1], $countries))
            $country  = '/'.$parts[1];
        //print_r($parts);exit;
        
        $app->params['fullBaseUrl'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '/';
        $app->params['fullBaseUrlLang'] = 'https://' . $_SERVER['HTTP_HOST'] . $country . '/';
        if(YII_ENV_TEST){
            $app->params['fullBaseUrl'] = "http://".$_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'].'/';  
            $app->params['fullBaseUrlLang'] = "http://".$_SERVER['HTTP_HOST']. $country.'/'; 
        }
        
        //echo $app->params['fullBaseUrl']; die;

        if(in_array($parts[1], $countries)){
            $app->params['countryPrefix'] = '/'.$parts[1];

            $_routes = [];
            foreach($routes as $index => $route){
                if(($index == '')){
                    $_routes["/<country:[A-Za-z]+>"] = 'site/index';
                } else{
                    $_routes["/<country:[A-Za-z]+>/{$index}"] = $route;
                }
            }
            $app->urlManager->addRules($_routes);

            $code = $parts[1];

            
            $app->session->set('hreflang', $code);
            $hreflangs = array_column($app->params['country_langs'], 'code');
            $clave = array_search($code, $hreflangs);
            if ($clave == '') {
                $currency = 'MXN';
            } else {
                $hreflang = $app->params['country_langs'][$clave];
                $currency = $hreflang['currency'];
            }
            $app->session->set('currency', $currency);
        } else {
            $app->params['countryPrefix'] = "";
            
            $app->urlManager->addRules($routes);

            $app->session->set('hreflang', '');
            $app->session->remove('hreflang');
            //$app->session->set('currency', 'MXN');
        }

        $completeUrl = '';
        $init = $app->params['countryPrefix'] == '' ? 1 : 2;
        if (count($parts)>2) {
            for ($i=$init; $i<count($parts); $i++) {
                $completeUrl .= $parts[$i] . '/';
            }
        } else {
            $completeUrl .= $parts[1] . '/';
        }
        $completeUrl = substr($completeUrl, 0, -1);
        $hreflangs = array_column($app->params['country_langs'], 'code');
        if (in_array($completeUrl, $hreflangs)) {
            $completeUrl = '';
        }
        $app->params['completeUrl'] = $completeUrl;
        // echo $app->params['completeUrl'];
        // print_r($parts);
        // exit;
    }
}