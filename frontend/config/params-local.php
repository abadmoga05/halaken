<?php
    /*define('rezdy', '2e23cd0353c6497496a0ae282e573dd2');
    define('appFareharbor', '4c561044-dc47-4afc-ba82-ef96ff86aed6');
    define('userFareharbor', '0e073f97-c940-47b6-aa53-a6811e347b15');
    define('staging', 'https://api.rezdy.com/');
    define('urlFareharbor', 'https://fareharbor.com/');*/

    define('rezdy','01844040cd724cefa5d5be0e64a777fb');
    define('appFareharbor','cc803edf-5138-49d2-ad58-97944d2b8e67');
    define('userFareharbor','034275dc-e6d9-40b1-bbe1-de0a7d2ea1a5');
    define('staging','https://api.rezdy-staging.com/');
    define('urlFareharbor','https://demo.fareharbor.com/');

return [
	'most-popular' => false,
	'pdf_tmp' => '/tmp',
    'enable_local_email' => 1, 
    'services' => [
        'email' => [
            'client' => 'TUE-Frontend-prod-clientMail:28'
        ]
    ],
    'activateCouponSection' => true,
     'searchService' => '/ajax/query',
    'home.version' => 2,
    'design.list.tour.version' => 2,
    'design.detail.tour.version' => 2,
    'design.passenger.information.version' => 2,
    'design.shopping.cart.version' => 2,
    'design.payment.version' => 2,
    'design.email.version' => 2,
    'promotion_active' => false,
    'banorte' =>[
	    'msi' => false
    ],
    'trustivity' => [
        'version' => 'v1',
        'api_key' => 'YEnTArBLjnBU8Lt4oZ55CdrpbWg1hSM6',
        'url_tienda' => 'tuexperiencia.com',
        'url' => [
            'sendinfo_production' => 'https://www.trustivity.es/request/order/index/',
            'sendinfo_develop' => 'https://www.trustivity.es/request/order/test/',
            'get_rating' => 'https://trustivity.es/api/general/get/',
            'get_reviews' => 'http://trustivity.es/api/producto/get_stars/',
        ]
    ],
    'ekomi.reviews.version' => 2, // start params ekomi
    'ekomi.api.product' => 'http://api.ekomi.de/v3/getProductfeedback',
    'ekomi.api.key' => '127647|a519a53f5f424ab2815725bef',
    'ekomi.api.version' => 'cust-1.0.0', // end params ekomi
    'reviews.provider.is.ekomi' => false,
    'listado.limit' => 15,
    'checkOutFlow' => 'v2',
    'activePaymentView' => true,
    'useIpService'=>false,
    'country_cuotas' => ['MX', 'CO', 'PE'],
    'images' => ['bucket' => 'cdn2.tuexperiencia.com'],
    'assets_version' => 217,
    'activeGtagVLC' => true,
    'showTourListActivitiesView' => true,
    'showTourListRegionsView' => true,
    'showBannerApp' => true,
    'cdnUrl' => 'https://d3tf9yuhsp2bpn.cloudfront.net',
    'cdn' => 'https://d3tf9yuhsp2bpn.cloudfront.net',
    'urlManagerFrontEnd' => 'http://frontend.tuexperiencia.com',
    'domain' => 'https://www.tuexperiencia.com',
    'notify' => 'marcoantonio@tuexperiencia.com',
    'notify2' => 'marcoantonio@tuexperiencia.com',
    'baseCurrency' => 'USD',
    'baseCountry' => 'USA',
    'provider' => 'https://extranet.tuexperiencia.com/',
    'agency' => 'https://extranet.tuexperiencia.com/index.php?r=site/agency',
    'affiliate' => 'https://extranet.tuexperiencia.com/index.php?r=site/affiliate',
    'agency_panel' => 'https://extranet.tuexperiencia.com/',
    'affiliate_panel' => 'https://extranet.tuexperiencia.com/',
    'whatsText' => '¿dudas?',
    'facebookText' => '&#xa1;Conoce nuestras promociones!',
    'showFBMessengerPopup' => true,
    'paypal' => [
	    'client_id' => 'AagbuRZR3a_kL_4H7k_43J8Kysqyj6RCQV7xUcYz44CXHGTSTlu_uAfK8iZxQm1GmXzYUZrnKZnIe3Gy',
	    'secret' => 'EAQ_OlVatdboFW7gRSM3L6dQYfZzxEmqr1gtktgE9h9iy6uc9dJqMCFGaz1rSs3munrau2CY3QLP86EI',
	    'uri' => 'https://api.sandbox.paypal.com',
        'minMSI' => '500',
        'MSISelected' => 1,
        'es' => [
	        'client_id' => 'AZlSKIN4z3_Qe-JbcQXDy6zM1AsKB4sYuHpc0fnOYkzn1ESPZnh7ecsN8y8tY4cgZUAHakuAa2G6dQta-QiU4p',
			'secret' => 'EKlMLT91WBPN_djT6P0SCZii2imIyGd_rZvrD15uoWm_kfz9oRRbDTzp2ng5s1U-jyMnwsQzaM9s_g3R',
        ]
    ],
	  'payment_service' => 'https://4yltl659k1.execute-api.us-east-1.amazonaws.com/prod',
    'AWS' =>[
        'key' => 'AKIAJ2ZD4K337WLK5IVA',
        'secret' => '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'
    ],
    'mercadopago' => [
	    'enabled' => false,
        'publicKey' => 'APP_USR-5ff9b916-a9b1-4889-a666-5b263b704364',
        'accessToken' => 'APP_USR-2804768970755883-043018-927c5e877ce4e6c3e241d9d1cb142d48-431739226'
    ],
    'preserve_cop' => false,
    'ACCOUNT' => [
        'module_enabled' => false,
        'API_ENDPOINT' => 'http://rest.tuexperiencia.com'
    ],
    'culqi' => [
          'public_key' => 'pk_test_Bw0mW1EVR0mFFGBU',
          'secret_key' => 'sk_test_y9On16UnM6teh2tz',
      ]
    ,
    'ebanx' => [
        'integration-key' => 'test_ik_61vM_042GK86BdnQTwRKSA',
        'integration' => 'live_ik_6iPtqAlMdn9xhTKGTKFpdA',
        'testMode' => true,
    ]
    ,'conekta' => [
        'key' => 'key_ezshtDL6yVyC5C6rH27BjQ',
        'api_version' => '2.0.0',
    ],
    'payu' => [
        'merchantId' => '508029',
        'api_login' => 'pRRXKOl8ikMmt9u',
        'api_key' => '4Vj8eK4rloUd272L48hsrarnUA',
        'accountId' => '512321',
        'urlPayU' => 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/',
        'country' => 'Colombia',
        'url_service_payment' => 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi'
    ],
    'bbva' => [
        'merchant_code' => '350305769',
        'merchant_terminal' => '1',
        'merchant_currency' => '978', //EUROS
        'secret_key' => 'sq7HjrUOBfKmC576ILgskD5srU870gJ7',
        'urlBBVA' => 'https://sis-t.redsys.es:25443/sis/realizarPago'
    ],
    'available_gateway' => ['USD', 'MXN','EUR','COP'],
    'external' => [
        'rezdy' => [
            'endpoint' => 'https://api.rezdy.com/v1',
            'enabled' => false,
            'key' => '2e23cd0353c6497496a0ae282e573dd2'
        ],
        'fareharbor' =>[
            'app' => '4c561044-dc47-4afc-ba82-ef96ff86aed6',
            'user' => '0e073f97-c940-47b6-aa53-a6811e347b15',
            'endpoint' => 'https://fareharbor.com'
      ],
    ],
];