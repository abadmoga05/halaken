<?php
//comment demo
setlocale(LC_ALL,"es_ES");

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$db = require __DIR__ . '/db.php';

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'modules' => [
        'debug' => 'yii\debug\Module',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'bootstrap' => [
        'frontend\extensions\components\FrontendBootstrap'
    ],
    'components' => [
        'mail' => [
            'class'            => 'zyx\phpmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'config'           => [
                'mailer'     => 'smtp',
                'host'       => 'mail.tuexperiencia.com',
                'port'       => '587',
                //'smtpsecure' => 'ssl',
                'smtpauth'   => true,
                'username'   => 'cto@tuexperiencia.com',
                'password'   => 'Huesca0106',
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'tuef',
        ],
        'log' => [
            
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'ajax/<action:\w+>' => 'ajax/<action>',
            ],
        ],
    ],
    'params' => $params,
];