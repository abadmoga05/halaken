<?php

namespace frontend\controllers;

use Yii;
use yii\web\Response;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\helpers\LayoutHelper as Layout;
use DateTime;

use yii\web\Controller;
/**
 * Clase del controlador account,
 * Es la clase del controlador.
 *
 */
class AccountController extends Controller
{
     /**
     * Acción principal del controlador principal,
     * Es la página login del sitio.
     *
     * @return yii\base\View
     */
    public function actionLogin()
    {
     
        $baseUrl = Url::base(true);

        $this->view->params['menu'] = 'account';
        $this->view->title = '▷ Tu cuenta【' . date('Y') . '】|🥇NB8';

        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('login', [$js]);
        $this->layout = 'main';

        return $this->render('login', [
            'baseUrl' => $baseUrl,
        ]);
    }
     /**
     * Acción ingreso a la cuenta,
     * Es la página login para peticion de acceso a la cuenta.
     *
     * @return yii\base\View
     */
    public function actionSendLogin()
    {
        $data = Yii::$app->request->post();
        $client = new Clients;

        $client->email = $data['email'];
        $client->password = crypt($data['password'], Yii::$app->params['salt']);
        $user = Clients::find()->where(['email' => $client->email])->AndWhere(['password' => $client->password])->one();

        if (!empty($user)) {
            //Salva Ultimo inicio de session
            $login = Clients::findOne($user->id);
            $login->last_login = date('Y-m-d');
            $login->save();
            $session = Yii::$app->session;
            $session->set('username', $data['email']);
            $session->set('token', $user->accessToken);
            $session->set('token_s_expire', time() + 3500);
            return $this->redirect(['/mi-cuenta']);
        } else {
            echo 'Ha ocurrido un error al ingresar datos, redireccionando ...';
            echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
        }
    }
    /**
     * Funcion para generar la key,
     * Es la funcion para generar la key aleatoria.
     * @param mixed $str  Cadena de valores.
     * @param mixed $long Largo de la cadena.
     * @return yii\base\View
    */
    public function randKey($str = '', $long = 0)
    {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for ($x=0; $x<$long; $x++) {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }
    /**
     * Acción registrarse del controlador principal,
     * Es la página de registro del sitio.
     *
     * @return yii\base\View
     */
    public function actionRegister()
    {
        $baseUrl = Url::base(true);
        $this->view->title = '▷ Registrate con nosotros【' . date('Y') . '】|🥇Tu Experiencia';

        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('register', [$js]);
        $this->layout = 'main';
        
        return $this->render('register', [
            'baseUrl' => $baseUrl,
        ]);
    }
    /**
     * Acción de envio de email del controlador para su validacion,
     * Es la página donde se envia email de activacion de cuenta.
     *
     * @return yii\base\View
     */
    public function actionSendRegistration()
    {
        $baseUrl = Url::base(true);
        $data = Yii::$app->request->post();
        //Preparamos la consulta para guardar el usuario
       
        $client = new Clients;
       
        $client->name = $data['name'];
        $client->last_name = $data['last_name'];
        $client->email = $data['email'];
        $client->phone = $data['phone_number'];
        $client->password = $data['password'];
        $client->activate = 0;
        $client->date_register = date('Y-m-d');
        $client->last_login = date('Y-m-d');
        $client->date_change = date('Y-m-d');
        //Encriptamos el password
        $client->password = crypt($client->password, Yii::$app->params['salt']);
        //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
        //clave será utilizada para activar el usuario
        $client->authKey = $this->randKey('abcdef0123456789', 200);
        //Creamos un token de acceso único para el usuario
        $client->accessToken = $this->randKey('abcdef0123456789', 200);
        $cliente = Clients::find()->where(['email' => $client->email])->one();
        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('register', [$js]);
        $this->layout = 'main';

        if (!empty($cliente)) {
            $error = 'Este usuario ya tiene una cuenta registrada...';
            //echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
            return $this->render('register', [
                'baseUrl' => $baseUrl,
                'error' => $error
            ]);
        } else {
            //Si el registro es guardado correctamente
            if ($client->insert()) {
                //Nueva consulta para obtener el id del usuario
                //Para confirmar al usuario se requiere su id y su authKey
                $user = Clients::find()->where(['email' => $client->email])->one();
                $id = urlencode($user->id);
                $authKey = urlencode($user->authKey);
        
                $subject = 'Confirmar registro';
                $body = '<h1>Haga click en el siguiente enlace para finalizar tu registro</h1>';
                $body .= "<a href='" . $baseUrl . "/validar-cuenta?id=" . $id . "&authKey=" . $authKey . "'>Confirmar</a>";
                //Enviamos el correo
                Yii::$app->mail->compose()
                ->setTo($user->email)
                ->setFrom(['antonio@tuexperiencia.com'  => 'Tu Experiencia Contacto'])
                ->setSubject($subject)
                ->setHtmlBody($body)
                ->send();
                $info = 'Se envió un correo electrónico para su validación.';
                //echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
                return $this->render('register', [
                    'baseUrl' => $baseUrl,
                    'info' => $info
                ]);
            } else {
                $error = 'Ha ocurrido un error al llevar a cabo tu registro.';
                //echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('registrarse') . "'>";
                return $this->render('register', [
                    'baseUrl' => $baseUrl,
                    'error' => $error
                ]);
            }
        }
    }
    /**
     * Acción de recibo de datos del email para su validacion de acceso,
     * Activa la cuenta del cliente y le permite el accesso.
     *
     * @return yii\base\View
     */
    public function actionConfirmUser()
    {
        $client = new Clients;
        if (Yii::$app->request->get()) {
            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET['id']);
            $authKey = $_GET['authKey'];
    
            if ((int) $id) {
                //Realizamos la consulta para obtener el registro
                $model = $client->find()->where('id=:id', [':id' => $id])->andWhere('authKey=:authKey', [':authKey' => $authKey]);
 
                //Si el registro existe
                if ($model->count() == 1) {
                    $activar = Clients::findOne($id);
                    $activar->activate = 1;
                    if ($activar->update()) {
                        echo 'Enhorabuena registro llevado a cabo correctamente, redireccionando ...';
                        echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
                    } else {
                        echo 'Ha ocurrido un error al realizar el registro, redireccionando ...';
                        echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
                    }
                } else {//Si no existe redireccionamos a login
                    return $this->redirect(['login']);
                }
            } else {//Si id no es un número entero redireccionamos a login
                return $this->redirect(['login']);
            }
        }
    }
    /**
     * Accion que muestra informacion de usuario del controlador
     * Muestra los datos generales del usuario.
     * @return yii\base\View
     */
    public function actionUserInformation()
    {
        $baseUrl = Url::base(true);
        $session = Yii::$app->session;

        $this->view->title = '▷ Mi cuenta【' . date('Y') . '】|🥇Tu Experiencia';

        $email =  $session->get('username');
        $token = $session->get('token');
        $expiration = $session->get('token_s_expire');
        $hasExpired = empty($expiration) ? true : $expiration <= time();
         
        $page = intval(Yii::$app->request->get('page'));
        $type = Yii::$app->request->get('type');

        $activities = $this->getActivities($email, $type, $page);
        
        //Agency module
        $isAgency = Agency::IsActive();
        $commission = $isAgency ? Agency::GetAgencyCommission(Agency::GetSession()->id, number_format(date('m')), date('Y')) : 0;
        $commission = ($commission==0) ? 18 : $commission;
        $agency = _Agency::findOne(['id' => Agency::GetSession()->id]);

        if (!$token || $hasExpired) {
            $session->close();
            return $this->redirect(['/inicia-sesion']);
        }

        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('user-information', [$js]);
        $this->layout = 'main';
        return $this->render('user-information', $activities);
    }
    /**
     * Accion que muestra datos personales del cliente del controlador y los envia a la vista.
     * Muestra datos personales del cliente y envia vista.
     * @return yii\base\View
     */
    public function actionUpdate()
    {
        $baseUrl = Url::base(true);
        $this->view->title = '▷ Actualizar información【' . date('Y') . '】|🥇Tu Experiencia';
        $session = Yii::$app->session;

        $email =  $session->get('username');
        $token = $session->get('token');
        $expiration = $session->get('token_s_expire');
        $hasExpired = empty($expiration) ? true : $expiration <= time();

        if (!$token || $hasExpired) {
            $session->close();
            return $this->redirect(['/inicia-sesion']);
        }

        $user = Clients::find()->where(['email' => $email])->one();
        if (!empty($user)) {
            $userData = $user;
        } else {
            $error = 'No se ha podido obtener la información de la cuenta. Por favor intente más tarde';
        }

        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('update-information', [$js]);
        $this->layout = 'main';
        
        return $this->render('update', [
            'userData' => $userData,
            'error' => $error
        ]);
    }
    /**
     * Accion que actualiza datos personales del cliente en la base y envia vista
     * Actualiza datos personales del cliente y envia vista.
     * @return yii\base\View
     */
    public function actionUpdateUserInformation()
    {
        $data = Yii::$app->request->post();
        
        $session = Yii::$app->session;
        $info = '';
        $error = '';

        $email =  $session->get('username');
        $token = $session->get('token');
        $expiration = $session->get('token_s_expire');
        $hasExpired = empty($expiration) ? true : $expiration <= time();

        if (!$token || $hasExpired) {
            $session->close();
            $error = 'Tu Sesion termino, favor de volver a iniciar sesion, redireccionando ...';
            echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
        }

        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('update-information', [$js]);
        $this->layout = 'main';

        
        $id = $data['id'];
        $model = Clients::find()->where('id=:id', [':id' => $id]);
        
        if ($model->count() == 1) {
            $clients = Clients::findOne($id);
            $clients->name = $data['name'];
            $clients->last_name = $data['last_name'];
            $clients->phone = $data['phone_number'];
            $clients->date_change = date('Y-m-d');
            if ($clients->save()) {
                $info = 'Tus datos se actualizarón correctamente';
                $userData = $clients;
                return $this->render('update', [
                    'userData' => $userData,
                    'info' =>$info
                ]);
            } else {
                $error = 'Ocurrio un error al actualizar los datos';
                return $this->render('update', [
                    'userData' => $userData,
                    'error' => $error
                ]);
            }
        } else {
            $error = 'No se ha podido obtener la información de la cuenta. Por favor intente más tarde';
            return $this->render('update', [
                'userData' => $userData,
                'error' => $error
            ]);
        }
    }
    public function actionUserChangePassword()
    {
        $this->view->title = '▷ Cambiar contraseña【' . date('Y') . '】|🥇Tu Experiencia';
        
        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('user-change-password', [$js]);
        $this->layout = 'main';
        
        return $this->render('user-change-password');
    }

    public function actionUpdateUserChangePassword()
    {
        $session = Yii::$app->session;
        $data = Yii::$app->request->post();
        $baseUrl = Url::base(true);
        
        $email =  $session->get('username');
        $token = $session->get('token');
        $expiration = $session->get('token_s_expire');
        $hasExpired = empty($expiration) ? true : $expiration <= time();

        if (!$token || $hasExpired) {
            $session->close();
            echo 'Tu Sesion termino, favor de volver a iniciar sesion, redireccionando ...';
            echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
        }

        $user = Clients::find()->where(['email' => $email])->one();
        $id = urlencode($user->id);
        $authKey = urlencode($user->authKey);
        $digits = 5;
        $numero = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $user->clave = $numero;
        $user->activate = 0;
        $user->save();

        $subject = 'Cambio de Contraseña';
        $body = '<h1>Haga click en el siguiente enlace e ingresa tu clave para cambiar tu contraseña</h1>';
        $body .= '<h2>Clave: ' . $numero . '</h2>';
        $body .= "<a href='" . $baseUrl . "/cambiar-acceso?id=" . $id . "&authKey=" . $authKey . "'>Confirmar Cambiar</a>";
        //Enviamos el correo
        Yii::$app->mail->compose()
        ->setTo($user->email)
        ->setFrom(['antonio@tuexperiencia.com'  => 'Tu Experiencia Contacto'])
        ->setSubject($subject)
        ->setHtmlBody($body)
        ->send();

        echo 'Se envio un correo electrónico para su validación, redireccionando ...';
        echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('logout') . "'>";
    }
    
    public function actionChangePassword()
    {
        $baseUrl = Url::base(true);

        if (Yii::$app->request->get()) {
            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET['id']);
            $authKey = $_GET['authKey'];
    
            if ((int) $id) {
                //Realizamos la consulta para obtener el registro
                $model = Clients::find()->where('id=:id', [':id' => $id])->andWhere('authKey=:authKey', [':authKey' => $authKey]);
 
                //Si el registro existe
                if ($model->count() == 1) {
                    $dataUser = Clients::findOne($id);
                } else {
                    $error = 'No se encontro usuario';
                }
            }
        }
        $this->view->title = '▷ Cambiar contraseña【' . date('Y') . '】|🥇Tu Experiencia';

        $js = <<<JS
            $(()=>{
                $('.lazy').lazyload({
                    threshold:200,
                    effect:'fadeIn'
                });
            })
JS;
            Layout::loadStyleAndScript('change-password', [$js]);
            $this->layout = 'main';
            return $this->render('change-password', [
                'baseUrl' => $baseUrl,
                'error' => $error,
                'dataUser' => $dataUser
            ]);
    }

    public function actionRecoverPassword()
    {
        $this->view->title = '▷ Recuperar contraseña【' . date('Y') . '】|🥇Tu Experiencia';
      
        $js = <<<JS
        $(()=>{
            $('.lazy').lazyload({
                threshold:200,
                effect:'fadeIn'
            });
        })
JS;
        Layout::loadStyleAndScript('recover-password', [$js]);
        $this->layout = 'main';
        
        return $this->render('recover-password');
    }

    public function actionSendRecoverEmail()
    {
        $baseUrl = Url::base(true);
        $data = Yii::$app->request->post();
        $session = Yii::$app->session;
        
        $cliente = Clients::find()->where(['email' => $data['recover_email']])->one();
        if (empty($cliente)) {
            echo 'Este usuario no tiene una cuenta registrada, redireccionando ...';
            echo "<meta http-equiv='refresh' content='8; " . Url::toRoute('login') . "'>";
        } else {
            //Nueva consulta para obtener el id del usuario
            //Para confirmar al usuario se requiere su id y su authKey
            $user = Clients::find()->where(['email' => $cliente->email])->one();
            $id = urlencode($user->id);
            $authKey = urlencode($user->authKey);
            $digits = 5;
            $numero = rand(pow(10, $digits-1), pow(10, $digits)-1);
            $user->clave = $numero;
            $user->activate = 0;
            $user->save();
            
            $subject = 'Recuperacion de Contraseña';
            $body = '<h1>Haga click en el siguiente enlace e ingresa tu clave para recuperar tu contraseña</h1>';
            $body .= '<h2>Clave: ' . $numero . '</h2>';
            $body .= "<a href='" . $baseUrl . "/cambiar-acceso?id=" . $id . "&authKey=" . $authKey . "'>Confirmar Recuperar</a>";
            //Enviamos el correo
            Yii::$app->mail->compose()
            ->setTo($user->email)
            ->setFrom(['antonio@tuexperiencia.com'  => 'Tu Experiencia Contacto'])
            ->setSubject($subject)
            ->setHtmlBody($body)
            ->send();
            $info = 'Se envio un correo electrónico para su validación';
            $js = <<<JS
            $(()=>{
                $('.lazy').lazyload({
                    threshold:200,
                    effect:'fadeIn'
                });
            })
JS;
            Layout::loadStyleAndScript('recover-password', [$js]);
            $this->layout = 'main';
            return $this->render('recover-password', [
                'baseUrl' => $baseUrl,
                'info' => $info
            ]);
        }
    }

    public function actionSendChangePassword()
    {
        $data = Yii::$app->request->post();
        
        $session = Yii::$app->session;
        $info = '';
        $error = '';
        $password = crypt($data['password'], Yii::$app->params['salt']);

        $id = $data['id'];
        $model = Clients::find()->where('id=:id', [':id' => $id])->andWhere('clave=:clave', [':clave' => $data['recover_code']]);

        $js = <<<JS
            $(()=>{
                $('.lazy').lazyload({
                    threshold:200,
                    effect:'fadeIn'
                });
            })
JS;
            Layout::loadStyleAndScript('change-password', [$js]);
            $this->layout = 'main';

        if ($model->count() == 1) {
            $clients = Clients::findOne($id);
            
            $clients->password = $password;
            $clients->activate = 1;
            $clients->date_change = date('Y-m-d');
            if ($clients->save()) {
                $info = 'Tus datos se actualizarón correctamente';
                $userData = $clients;
                return $this->render('change-password', [
                    'userData' => $userData,
                    'info' =>$info
                ]);
            } else {
                $error = 'Ocurrio un error al actualizar los datos';
                return $this->render('change-password', [
                    'userData' => $userData,
                    'error' => $error
                ]);
            }
        } else {
            $error = 'No se ha podido obtener la información de la cuenta. Revise los datos enviados';
            return $this->render('change-password', [
                'userData' => $userData,
                'error' => $error
            ]);
        }
    }

    public function actionUpdateTourCancel()
    {
        $data = Yii::$app->request->post();
        $confirmation= $data['confirmacion'];
        $this->layout = 'email';
        $responseData = [];
        
        try {
           TourBooking::updateAll([
                'id_tour_booking_status' => TourBooking::CANCELED
            ], ['confirmation'=>$confirmation]);
            
            $bookings = TourBooking::findAll(['confirmation'=> $confirmation]);
            $oneBooking = TourBooking::findOne(['confirmation'=>$confirmation]);

            $lead = TourBookingLead::findOne(['id_tour_booking'=>$oneBooking->id]);
            
            //buscamos el valor de descuento, porcentaje de coupon, para agregarlo en vouchers y vista
            $fullTotal = $oneBooking->getTotalBookingDiscount($oneBooking);
            $fullTotal = number_format($fullTotal, 2, '.', '');
            $subTotal = TourBooking::Total($oneBooking->key);
            $subTotal = number_format($subTotal, 2, '.', '');
            //email del cliente
            $emailCliente = $lead->email;
            
            
            foreach ($bookings as $index => $booking) {
                $allTours[$booking->tour->contract->company->id][] = $booking;
            }
            foreach ($allTours as $index => $books) {
                $contacts = CompanyContact::findAll(['id_company'=>$index, 'notify_booking'=> 1]);
                if ($oneBooking->id_agency) {
                    $agencyContact= AgencyContact::findAll(['id_agency'=>$oneBooking->id_agency, 'notify_booking'=> 1]);
                }

                $data= [
                    'booking' => $booking,
                    'optional' => $booking->optional,
                    'tours' => $books,
                    'total' => 0,
                    'print' => false,
                    'lead' => $lead,
                    'currency' => $booking->tour->contract->currency->code,
                    'symbol'    => $booking->tour->contract->currency->symbol,
                    'passangers' => $booking->passengersTotal(true)
                ];

                $html = $this->render('mailCancel', $data);
                $agency_html = $this->render('mailCancel', $data);
            
                if ($emailCliente) {
                    if (YII_ENV == 'prod') {
                        Yii::$app->mail->compose()
                            ->setFrom(['cancelaciones@tuexperiencia.com' => 'Tu Experiencia'])
                            ->setTo($emailCliente)
                            ->setBCC(Yii::$app->params['notify'])
                            ->setBCC(Yii::$app->params['notify2'])
                            ->setSubject('Notificación de Cancelación: ' . $confirmation)
                            ->setHtmlBody($html)
                            ->send();
                    }
                }
                foreach ($contacts as $contact) {
                    if (YII_ENV == 'prod') {
                        Yii::$app->mail->compose()
                            ->setFrom(['cancelaciones@tuexperiencia.com' => 'Tu Experiencia'])
                            ->setTo($contact->email)
                            ->setBCC(Yii::$app->params['notify'])
                            ->setBCC(Yii::$app->params['notify2'])
                            ->setSubject('Notificación de Cancelación: ' . $confirmation)
                            ->setHtmlBody($html)
                            ->send();
                    }

                }
                if ($oneBooking->id_agency) {
                    foreach ($agencyContact as $contact) {
                        if ($contact->notify_booking) {
                            if (YII_ENV == 'prod') {
                                Yii::$app->mail->compose()
                                ->setFrom(['cancelaciones@tuexperiencia.com' => 'Tu Experiencia'])
                                ->setTo($contact->email)
                                ->setBCC(Yii::$app->params['notify'])
                                ->setBCC(Yii::$app->params['notify2'])
                                ->setSubject('Notificación de Cancelación: ' . $confirmation)
                                ->setHtmlBody($agency_html)
                                ->send();
                            }
                        }
                    }
                }
            }
            $responseData = [
                'code' => 200,
                'message' => 'En proceso de cancelación'
            ];
        } catch (Exception $e) {
            $responseData = [
                'code' => 400,
                'message' => 'Ocurrio un error: ' . $e
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $responseData;
    }

    private function getActivities($email, $type = 'current', $page = 1)
    {
        $bookings = [];
        $count = 0;
        $start = 0;
        $currentPage = $page;
        $pageSize = 2;
        $offset = $currentPage > 0 ? ($currentPage - 1) * $pageSize: 0;
        //$email='ALEDAPLA@GMAIL.COM';PRUEBAS
        $toursLeads = ArrayHelper::getColumn(
            TourBookingLead::find()->where(['email' => $email])->all(),
            'id_tour_booking',
            false
        );

        if ($type == 'history') {
            $totalBookings =(TourBooking::find()->where(['in', 'id', $toursLeads])
                ->andWhere(['<', 'travel_date', new Expression('NOW()')])
                ->andWhere(['not', ['confirmation' => null]])
                ->all());

            $count = sizeof($totalBookings);

            $foundBookings = TourBooking::find()->where(['in', 'id', $toursLeads])
                ->andWhere(['<', 'travel_date', new Expression('NOW()')])
                ->andWhere(['not', ['confirmation' => null]])
                ->limit($pageSize)->offset($offset)->all();

            foreach ($foundBookings as $tourBooking) {
                $booking = $this->getUserBooking($tourBooking->key);
                array_push($bookings, $booking);
            }
        } elseif ($type == 'canceled') {
            $totalBookings =(TourBooking::find()->where(['in', 'id', $toursLeads])
                ->andWhere(['id_tour_booking_status'=> TourBooking::CANCELED])
                ->andWhere(['not', ['confirmation' => null]])
                ->all());

            $count = sizeof($totalBookings);

            $foundBookings = TourBooking::find()->where(['in', 'id', $toursLeads])
                ->andWhere(['id_tour_booking_status' => TourBooking::CANCELED])
                ->andWhere(['not', ['confirmation' => null]])
                ->limit($pageSize)->offset($offset)->all();

            foreach ($foundBookings as $tourBooking) {
                $booking = $this->getUserBooking($tourBooking->key);
                array_push($bookings, $booking);
            }
        } else {
            $totalBookings =(TourBooking::find()->where(['in', 'id', $toursLeads])
                ->andWhere(['>', 'travel_date', new Expression('NOW()')])
                ->andWhere(['not', ['id_tour_booking_status' => TourBooking::CANCELED]])
                ->andWhere(['not', ['confirmation' => null]])
                ->all());

            $count = sizeof($totalBookings);

            $foundBookings = TourBooking::find()->where(['in', 'id', $toursLeads])
                ->andWhere(['>', 'travel_date', new Expression('NOW()')])
                ->andWhere(['not', ['id_tour_booking_status' => TourBooking::CANCELED]])
                ->andWhere(['not', ['confirmation' => null]])
                ->limit($pageSize)->offset($offset)->all();

            foreach ($foundBookings as $tourBooking) {
                $booking = $this->getUserBooking($tourBooking->key);
                array_push($bookings, $booking);
            }
        }

        $total_pages = ceil($count / $pageSize);
        $end = $total_pages;

        $adjacents = 3;

        //Here we generates the range of the page numbers which will display.
        if ($total_pages <= (1+($adjacents * 2))) {
            $start = 1;
            $end   = $total_pages;
        } else {
            if (($page - $adjacents) > 1) {
                if (($page + $adjacents) < $total_pages) {
                    $start = ($page - $adjacents);
                    $end   = ($page + $adjacents);
                } else {
                    $start = ($total_pages - (1+($adjacents*2)));
                    $end   = $total_pages;
                }
            } else {
                $start = 1;
                $end   = (1+($adjacents * 2));
            }
        }
        
        return [
            'bookings' => $bookings,
            'total_pages' => $total_pages,
            'page' => $page,
            'start' => $start,
            'end' => $end,
            'type' => $type
        ];
    }

    protected function getUserBooking($key)
    {
        setlocale(LC_MONETARY, 'en_US');
        $session = Yii::$app->session;
        $bookings = TourBooking::findAll(['key'=> $key]);
        $oneBooking = $bookings[0];
        $oneRate = $oneBooking->tourBookingRates[0];
        $transctionAmount = TourBooking::Total($key);
        $itemsStr = [];
        $valida = false;
       
        
        if ($oneRate->discount) {
            $discount = (($oneRate->discount  / 100) * $transctionAmount);
            $nombreDescuento = "Cupon de {$oneRate->discount}%";
            $descuento = number_format($discount, 2, '.', '');
            $totalConDesc =number_format($transctionAmount - $discount, 2, '.', '');
            $transctionAmount = number_format($transctionAmount - $discount, 2, '.', '');
        }

        $transctionAmount = 0;
        foreach ($bookings as $booking) {
            $itemTotal = number_format($booking->ItemTotal(), 2, '.', '');
            $currencySymbol = $booking->getCurrency();
            $currency = $booking->getCurrencyValue();
            $model = TourDetails::findOne(['id_tour' => $booking->tour->id]);
            $tour = $model->tour;
            $image = $tour->getCover();

            $seasons_ = TourSeason::find()
            ->where(' CURDATE() >= start_on')
            ->andWhere(' CURDATE() <= end_on')
            ->andWhere('id_tour_optional', $booking->optional->id)
            ->all();
    
            $seasonsIds = ArrayHelper::getColumn($seasons_, 'id');

             //Validacion de horario de cut-off 
            $getCiudad = City::find()->where(['id' => $tour->id_city])->all();
            $time_zone_id = max(ArrayHelper::getColumn($getCiudad, 'time_zone_id'));

            //traigo pais de donde ingresa
            $geoplugin = new GeoPlugin();
            $geoplugin->locate();
            $dayCutoff = 0;

            //echo 'Geolocation results for $geoplugin->ip: <br />\n';
            if ($geoplugin->ip != '127.0.0.1') {
                $timezoneCliente = $geoplugin->timezone;
                Yii::$app->timeZone = $timezoneCliente;
                $timezones = Yii::$app->getTimeZone();
                $horaCliente = strtotime(date('Y-m-d H:i:s', time()));
            } else {
                Yii::$app->timeZone = 'America/Mexico_City';
                $timezones = Yii::$app->getTimeZone();
                $horaCliente = strtotime(date('Y-m-d H:i:s', time()));
            }
            $clienteHora = date('Y-m-d H:i:s', $horaCliente);

            //traigo zona horaria de tour
            if ($time_zone_id > 0) {
                $getZonaId = TimeZone::find()->where(['time_zone_id' => $time_zone_id])->all();
                $timezone =  max(ArrayHelper::getColumn($getZonaId, 'zone'));
                Yii::$app->timeZone = $timezone;
            } else {
                $timezone = Yii::$app->getTimeZone();
                Yii::$app->timeZone = $timezone;
            }
            Yii::$app->setTimeZone($timezone);
            $dateTravel = $booking->travel_date;
            
            $horaLocal = strtotime($booking->travel_date);
            $localHora = date('Y-m-d H:i:s', $horaLocal);

            //resto a una fecha la otra
            $segundos_diferencia = strtotime($localHora) - strtotime($clienteHora);

            //convierto segundos en horas
            $horas_diferencia = round(($segundos_diferencia / 3600), 0);

            if ($horas_diferencia >= 0) {
                $nuevafecha = strtotime('+' . $horas_diferencia . 'hour', strtotime($clienteHora));
            } else {
                $horas_diferencia = $horas_diferencia * -1;
                $nuevafecha = strtotime('-' . $horas_diferencia . 'hour', strtotime($clienteHora));
            }
            //fecha sumando o restando horas dependiendo de la di¡ferencia de horario
            $nuevafecha = date('Y-m-d H:i:s', $nuevafecha);

            //fecha final sumandole el cutoff puesto por el proveedor
            $fecha = date('Y-m-d');
            $seg = date('s');
            $hours = TourAvailabilityHours::findAll(['id_season' => $seasonsIds]);
            $horas = ArrayHelper::getColumn($hours, 'hour');
            
            foreach ($horas as $index => $h) {
                $hora[] = trim($h);
            }
            //horario inicio tour
            $horaInicioTour = $fecha . ' ' . $hora[0] . ':' . $seg;
            
            $horaInicioTour =  strtotime('- 24 hour', strtotime($horaInicioTour));
            $horaInicioTour = date('Y-m-d H:i:s', $horaInicioTour);

            $cutoffTour = $model->tour->config->tourCutOff->value;
            $cutoffTour = $cutoffTour;
            $horaInicioLocal =  strtotime('+' . $cutoffTour . 'hour', strtotime($nuevafecha));
            $horaInicioLocal = date('Y-m-d H:i:s', $horaInicioLocal);

            
            $fecha1= new DateTime($horaInicioTour);
            $fecha2= new DateTime($horaInicioLocal);
            $diff = $fecha1->diff($fecha2);
            // El resultados sera 3 dias
            $horas_diferencia = ($diff->invert == 1) ? ' - ' . $diff->days . ' dias'  : $diff->days . ' dias';

            if ($horas_diferencia > 0) {
                $valida = true;
            } else {
                $valida = false;
            }
 

            $transctionAmount += $itemTotal;

            $itemsStr = (object) [
                'name' => $booking->tour->tourDetails->name,
                'image' => $image,
                'description' => $booking->optional->name,
                'descDescuento' => $nombreDescuento,
                'descuento' => $descuento,
                'quantity' => 1,
                'price' => $transctionAmount,
                'totalConDesc' => $totalConDesc,
                'sku' => 'TUE.' . $booking->tour->id . '.' . $booking->optional->id,
                'symbol' => $currencySymbol,
                'currency' => $currency['code'],
                'confirmation' => $booking->confirmation,
                'travel_date' => $booking->travel_date,
                'valida' => $valida,
                'status' => $booking->id_tour_booking_status
            ];
        }
    
        $transctionAmount = str_replace(',', '.', $transctionAmount);
    
        $transactions = (object) [
          'item_list' => [
            'items' => $itemsStr
          ],
        ];
        
        return $transactions;
    }

    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->set('username', '');
        $session->set('token', '');
        $session->close();
        return $this->redirect(['/inicia-sesion']);
    }
}