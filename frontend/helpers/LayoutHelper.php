<?php
namespace frontend\helpers;

use Yii;
use yii\helpers\Url;

/**
 * Utilerias
 */
class LayoutHelper
{
    
    /**
     * @var $baseUrl, path de front para acceder a los css´s y script´s
    */
    protected static $baseUrl = null;
    /**
     * @var $appView, instancia de Yii::$app->view
    */
    protected static $appView = null;
    /**
     * @var $bootstrap_version, Version de Bootstrap para Cargar
    */
    protected static $bootstrap_version = '4.4.1';
    /**
     * @var $handlebars_version, Version de Handlebars para Cargar
    */
    protected static $handlebars_version = '4.1.2';
    
    
    /**
     * @param string $view Nombre de de la vista para cargar Css´s y Scripts.
     * @param array $js Arreglo de javascript´s independientes que se desan cargar desde el controlador.
     * @param string $jsv Versiónde jquery que se desea cargar.
     * @return void Inicia variables generales e inicializa la carga de css´s y csript´s
    */
    public static function loadStyleAndScript(string $view = 'home', array $js = [], string $jsv = '3.4.1')
    {
        // Init $baseUrl to complete route
        self::$baseUrl = Url::base(true);
        self::$appView = Yii::$app->view;
        // Load Css and Js files for all views
        self::loadInit($view, $jsv);
        //Load Css and Js files for the request view
        self::selectLoadFiles($view, $js );
    }

    /**
     * @param string $view vista para cargar script y css.
     * @param string $js_version Versión de jquery que se desea cargar.
     * @return void Carga los css´s y script necesarios para el template inicial
    */
    public function loadInit(string $view, string $js_version = '3.4.1')
    {
        
        self::$appView->registerJsFile(self::$baseUrl . '/js/jquery-3.5.1.min.js', ['rel' => 'preload', 'as' => 'javascript', 'position' => \yii\web\View::POS_END]);
        self::$appView->registerJsFile(self::$baseUrl . '/js/jquery-ui.js', ['rel' => 'preload', 'as' => 'javascript','position' => \yii\web\View::POS_END]);
        self::$appView->registerJsFile(self::$baseUrl . '/js/popper.min.js', ['rel' => 'preload', 'as' => 'javascript','position' => \yii\web\View::POS_END]);
        self::$appView->registerJsFile(self::$baseUrl . '/js/bootstrap.min.js', ['rel' => 'preload', 'as' => 'javascript','position' => \yii\web\View::POS_END]);
        
        /*CSS*/
        self::$appView->registerCssFile(self::$baseUrl . '/css/bootstrap.min.css', ['rel'=>'stylesheet', 'as'=>'style', 'media' => 'screen', 'position' => \yii\web\View::POS_END]);
        self::$appView->registerCssFile(self::$baseUrl . '/css/jquery-ui.min.css', ['rel'=>'stylesheet', 'as'=>'style','media' => 'screen', 'position' => \yii\web\View::POS_END]);
        self::$appView->registerCssFile(self::$baseUrl . '/css/font-awesome.min.css', ['rel' => 'stylesheet', 'as' => 'style', 'media' => 'screen', 'position' => \yii\web\View::POS_END]);

        self::$appView->registerCssFile(self::$baseUrl . '/css/styles.css', ['rel' => 'stylesheet', 'as' => 'style', 'media' => 'screen', 'position' => \yii\web\View::POS_END]);

    }

    /**
     * @param string $view Nombre de la vista para cargar los css y script.
     * @param array $js Arreglo de javascript´s independientes que se desan cargar desde el controlador.
     * @return void Llama a la funcion necesaria para cargar los files dependiendo la vista
    */
    public function selectLoadFiles(string $view, array $js = [])
    {
        switch ($view) {
            case 'home': self::home();
                break;
            case 'login': self::login();
                break;
            
        }
    }
    /**
     * @return void Carga los css´s y script necesarios para la home
    */
    private static function home()
    {

        self::$appView->registerLinkTag([
            'rel' => 'canonical',
            'href' => substr(Yii::$app->params['fullBaseUrlLang'], 0, -1),
        ]);

        self::$appView->registerMetaTag([
            'name' => 'keywords',
            'content' => 'Agregar las keywords',
        ]);

        self::$appView->registerMetaTag([
            'name' => 'description',
            'content' => 'meta descripcion',
        ]);


    }
    /**
     * @return void Carga los css´s y script necesarios para la login
    */
    private static function login()
    {
        self::$appView->registerLinkTag(['rel' => 'canonical', 'href' => substr(Yii::$app->params['fullBaseUrl'], 0, -1),]);
        self::$appView->registerMetaTag(['name' => 'robots','content' => 'noindex,nofollow',]);
        
        self::$appView->registerMetaTag([
            'name' => 'description',
            'content' => 'Tour, Entradas, Excursiones y Visitas guiadas en español. Guías locales de habla hispana con los mejores precios.',
        ]);
        self::$appView->registerJsFile(self::$baseUrl . '/js/account.js?v=' . Yii::$app->params['assets_version'], ['as' => 'javascript', 'position' => \yii\web\View::POS_END]);
        self::$appView->registerJsFile(self::$baseUrl . '/js/paymentmethods/vendor/masking-input.js?v=' . Yii::$app->params['assets_version'], ['position' => \yii\web\View::POS_END]);
        self::$appView->registerJsFile(self::$baseUrl . '/js/home.min.js?v=' . Yii::$app->params['assets_version'], ['as' => 'javascript','position' => \yii\web\View::POS_END]);

        

        self::$appView->registerCssFile(self::$baseUrl . '/css/mi-cuenta.min.css?v=' . Yii::$app->params['assets_version'], ['rel'=>'stylesheet', 'as'=>'style', 'media' => 'screen']);
        self::$appView->registerCssFile(self::$baseUrl . '/css/font-awesome.min.css', ['rel'=>'stylesheet', 'as'=>'style', 'media' => 'screen']);
    
    }
    

}
